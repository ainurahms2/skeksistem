<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/lowongan-style.css'?>">
    <title>SKEK SYSTEM UINSA</title>
    <link rel="icon" type="image/png" href="<?php echo base_url();?>./assets/img/uinsa.png">
    
</head>
<body id="LoginForm">
    <div class="container">
        <div class="form-heading">
            <div class="login-form">
                <div class="main-div">
                    <div class="panel">
                        <h2>Skek Sistem Login</h2>
                        <p>Masukkan NIM/NIP dan Password anda</p>
                    </div>
                    <form id="Login" action="<?php echo base_url();?>login/proseslogin" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="nim" placeholder="NIM/NIP" name="username">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                            <!-- <input type="hidden" name="tampunglogin" id="tampunglogin"> -->
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>
