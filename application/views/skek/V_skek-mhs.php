
<div id="wrapper">
        <!-- SIDEBAR HOLDER -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Main Menu</h4>
            </div>
            <ul class="list-unstyled components">
                <li class="namabar">
                    <a href="<?php echo base_url();?>mahasiswa/index"><?php echo $this->session->userdata('nama');?></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>mahasiswa/index">Home</a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url();?>mahasiswa/inputview">Masukkan Data Skek</a>
                </li>
                <li class="logoutbar">
                    <a href="<?php echo base_url();?>login/logout">Log Out</a>
                </li>
            </ul>
        </nav>
        <div id="content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>mahasiswa/index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Masukkan Data Skek</li>
				</ol>
			</nav>
            <h2 style="text-align: center;"><?php echo $this->session->userdata('nama'); ?></h2>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#formulir">Formulir</a></li>
                <li><a data-toggle="tab" href="#rekapan">Rekapan</a></li>
            </ul>
                      
                <div class="tab-content">
                    <div id="formulir" class="tab-pane fade in active">
                        <form class="form-horizontal" id="input" name="input" action="<?php echo base_url();?>Mahasiswa/simpandatamhs" method="POST" enctype="multipart/form-data">
                            <h3>Formulir</h3>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="nama">Nama:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="namas" placeholder ="Nama" value="<?php echo $this->session->userdata('nama'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="nim">NIM:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="nim" placeholder="NIM" value="<?php echo $this->session->userdata('nim'); ?>">
                                    <input type="hidden" id="niphid" name="niphid">
                                </div> 
							</div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="prodi">Prodi:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="prodi" placeholder="Prodi" value="<?php echo $this->session->userdata('prodi'); ?>">
                                </div> 
							</div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="aspek">Aspek:</label>
                                <div class="col-sm-4">
                                    <select class="form-control" id="aspek" name="aspek">
                                            <option>Pilih Aspek</option>
                                            <?php foreach($aspek as $row): ?>
                                                    <option value=<?php echo $row['ID_ASPEK'].">".$row['ASPEK'];?></option>
                                            <?php endforeach; ?>
                                    </select>
                                </div>
							</div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="subaspek">Sub-Aspek:</label>
                                <div class="col-sm-4">
                                    <select class="subaspek form-control" id="subaspek" name="subaspek">
                                        <option select>Pilih Aspek terlebih dahulu</option>
                                    </select>
                                </div>
						    </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="tingkat">Tingkatan:</label>
                                <div class="col-sm-4">
                                    <select class="tingkat form-control" id="tingkat" name="tingkat">
                                        <option select>Pilih Sub-Aspek terlebih dahulu</option>
                                    </select>
                                </div>
						    </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="posisi">Posisi:</label>
                                <div class="col-sm-4">
                                    <select class="posisi form-control" id="posisi" name="posisi">
                                        <option>Pilih Tingkatan terlebih dahulu</option>
                                    </select>
                                </div>
						    </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="poin">Poin:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="poin form-control" id="poin" name="poin" placeholder="Poin">
                                    <input type="hidden" id="idrubrikhid" name="idrubrikhid">
                                </div>
						    </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="upload">Upload:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="sertif form-control" id="sertif" name="sertif">*masukkan link google drive <br> 
									<a style="color:blue;" href=<?php echo base_url()."/uploads/panduan_upload_skek.pdf" ?> target="_blank">cara upload file bukti</a>
                                </div>
								
                            </div>
							<button id="simpan" type="submit" style="margin: 0 0 0 41.5%;" class="btn btn-primary fas fa-save"> Simpan</button>
                        </form>
                    </div>
                    <div id="rekapan" class="tab-pane fade">
					    <h3>Rekapan Skek</h3>
						<div style="padding:5px;"></div>
                            <table class="table table-bordered table-striped dataTable no-footer" id ="tabelaspek" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Aspek Kegiatan</th>
                                        <th style="text-align:center;">Poin Total</th>
                                        <th style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
										<td>ASPEK KEAGAMAAN DAN MORAL PANCASILA</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspeka as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>mahasiswa/detilaspeka"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
										<td>ASPEK PENALARAN DAN IDEALISME</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekb as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>mahasiswa/detilaspekb"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
										<td>ASPEK KEPEMIMPINAN DAN LOYALITAS</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekc as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>mahasiswa/detilaspekc"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
										<td>ASPEK PEMENUHAN MINAT DAN BAKAT</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekd as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>mahasiswa/detilaspekd"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
										<td>ASPEK PENGABDIAN KEPADA MASYARAKAT</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspeke as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>mahasiswa/detilaspeke"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						    <div style="padding:7px;"></div>
                                <div class = "col-md-2 kotak">
                                    <label for="total_skek" class="total">Total Skek</label>
                                    <input type="text" class="form-control text-right" id="total_skek" value="<?php echo $totalaallaspek[0]['Total'] ?>">
                                </div>
                    </div>
                </div>
        </div>
    </div>
    <?php $this->view('skek/V_footer'); ?>

<script type="text/javascript">
    $(document).ready(function(){
        var aspek = "";
        var subaspek = "";
        var tingkatan = "";
        $('#aspek').change(function(){
            var aspek=$('#aspek').val();
            // console.log(aspek);
            $.ajax({
                url : "<?php echo base_url();?>mahasiswa/getsubaspek",
                method : "POST",
                data : {id_aspek: aspek},
                async : false,
                dataType : 'json',
                success: function(data){
                    console.log(data);
                    var html = '';
                    var i;
                    html += '<option>Pilih Sub-Aspek</option>';
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].ID_SUB_ASPEK+'>'+data[i].SUB_ASPEK+'</option>';
                    }
                    $('#subaspek').html(html);
                    // global = $('#subaspek').html();
                    // aspek = $("#aspek").val();
                    // console.log(global);
                    
                    
                }
            });
        });
        $('#subaspek').change(function(){
            var subaspek=$('#subaspek').val();
            // console.log(subaspek);
            $.ajax({
                url : "<?php echo base_url();?>mahasiswa/gettingkat",
                method : "POST",
                data : {id_sub_aspek: subaspek},
                async : false,
                dataType : 'json',
                success: function(data){
                    console.log(data);
                    var html = '';
                    var i;
                    html += '<option>Pilih Tingkat</option>';
                    for(i=0; i<data.length; i++){
                        html += '<option value=' + data[i].ID_TINGKAT + '>' + data[i].TINGKAT + '</option>';
                    }
                    $('#tingkat').html(html);
                    // subaspek = $('#subaspek').val();
                    // console.log(subaspek);
                     
                }
            });
        });
            $('#tingkat').change(function(){
                var aspek = $('#aspek').val();
                var subaspek = $('#subaspek').val();
                var tingkat=$('#tingkat').val();
                // console.log(posisi);
                $.ajax({
                    url : "<?php echo base_url();?>mahasiswa/getposisi",
                    method : "POST",
                    data : {id_aspek: aspek,id_sub_aspek: subaspek, id_tingkat: tingkat},
                    async : false,
                    dataType : 'json',
                    success: function(data){
                        console.log(data);
                        var html = "";
                        var i;
                        html += '<option>Pilih Posisi</option>';
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].ID_POSISI+'>'+data[i].POSISI+'</option>';
                        }
                        $('#posisi').html(html);
                    }
                });
            });
            $('#posisi').change(function(){
                var aspek = $('#aspek').val();
                var subaspek = $('#subaspek').val();
                var tingkat=$('#tingkat').val();
                var posisi=$('#posisi').val();
                var id_rubrik;
                // console.log(posisi);
                $.ajax({
                    url : "<?php echo base_url();?>mahasiswa/getpoin",
                    method : "POST",
                    data : {id_aspek: aspek,id_sub_aspek: subaspek, id_tingkat: tingkat, id_posisi: posisi, id_rubrik: id_rubrik},
                    async : false,
                    dataType : 'json',
                    success: function(data){
                        console.log(data);
                        // var html = "";
                        // $('#poin').val(data.POIN);
                        $("#poin").attr("placeholder",(data[0].POIN));
                        $("#idrubrikhid").attr("value",(data[0].ID_RUBRIK));                        

                        // document.getElementById("poin").value = data;
                        
                    }
                });
            });
            // $('#simpan').click(function(){
            //     var getaspek = $('#aspek').val();
            //     var getsubaspek = $('#subaspek').val();
            //     var gettingkat = $('#tingkat').val();
            //     var getposisi = $('#posisi').val();
            //     var getpoin = $('#poin').val();
            //     $.ajax({
            //         url : "",
            //         method : "POST",
            //         data : {id_aspek: aspek,id_sub_aspek: subaspek, id_tingkat: tingkat, id_posisi: posisi},
            //         async : false,
            //         dataType : 'json',
            //         success: function(data){
            //             console.log(data);
            //             // var html = "";
            //             // $('#poin').val(data.POIN);
            //             // $("#poin").attr("placeholder",(data[0].POIN));
            //             // document.getElementById("poin").value = data;
                        
            //         }
            //     });
            // });
    });
    // var data = $('#input').serialize();
    // console.log(data);
    // $('#input').submit(function(e){
    //     e.preventDefault();
    //     console.log(data);
    // })
</script>
</body>
</html>