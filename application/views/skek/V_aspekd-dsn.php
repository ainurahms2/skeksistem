<div id="wrapper">
        <!-- SIDEBAR HOLDER -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Main Menu</h4>
            </div>
            <ul class="list-unstyled components">
                <li class="namabar">
                    <a href="<?php echo base_url();?>dosen/index"><?php echo $this->session->userdata('nama');?></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>dosen/index">Home</a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url();?>dosen/validasiview">Validasi Skek</a>
                </li>
                <li class="logoutbar">
                    <a href="<?php echo base_url();?>login/logout">Log Out</a>
                </li>
            </ul>
        </nav>
        <div id="content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/index">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/validasiview">Validasi Skek</a></li>
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/permhs/<?php echo $getdetilnim[0]['NIM']?>">Detail Skek</a></li>                    
					<li class="breadcrumb-item active" aria-current="page">Status per Mahasiswa</li>
				</ol>
			</nav>
            <!-- <h2 style="text-align: center;"><?/*php echo $this->session->userdata('nama'); */?></h2> -->
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" href="#detailskeka">Detail Skek</a></li>
            </ul>
                <div class="form-group">      
                    <div class="tab-content">
                        <div id="detailskeka" class="tab-pane fade in active">
                            <h3>ASPEK PENALARAN DAN IDEALISME</h3>
                            <div style="padding:5px;"></div>
                                <table class="table table-bordered table-striped dataTable no-footer" id ="tabelperskek" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:1px;">No</th>
                                            <th style="width:500px;">Sub-Aspek</th>
                                            <th style="width:10px; text-align:center;">Tingkatan</th>
                                            <th style="width:5px;">Posisi</th>
                                            <th style="width:5px;">Poin</th>
                                            <th style="width:50px; text-align:center;">Status</th>
											<th style="width:50px; text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if($getaspekd->num_rows() > 0){
                                        $var=1;
                                        foreach($getaspekd->result() as $row)
                                        {   
                                    ?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $var?></td>
                                            <td><?php echo $row->SUB_ASPEK?></td>
                                            <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                                            <td style="text-align:center;"><?php echo $row->POSISI?></td>
                                            <td style="text-align:center;"><?php echo $row->POIN?></td>
                                            <td style="text-align:center;">
                                                <?php if($row->STATUS == 0){?>
                                                    <span class="label label-warning">
                                                        MENUNGGU
                                                    </span>
                                                <?php }else if($row->STATUS == 1){?>
                                                    <span class="label label-success">
                                                        DITERIMA
                                                    </span>
                                                <?php }else{?>
                                                    <span class="label label-danger">
                                                        DITOLAK
                                                    </span>
                                                <?php }?>
                                                    
                                            </td>
											<td style="text-align:center;">
												<a target="_blank" href="<?php echo $row->FILE_UPLOAD?>" class="btn btn-success fas fa-search btna buka-gambar"> Detail</a>
											</td>
                                        </tr>
                                    <?php 
                                    $var++;
                                        }
                                    }else{
                                    ?>
                                    <?php 
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <div style="padding:7px;"></div>
                                    <div class = "col-md-2 kotak">
                                        <label for="total_skek" class="total">Total Skek</label>
                                        <input type="text" class="form-control text-right" id="total_skek" value="<?php echo $totalaspekd[0]['Total'] ?>">
                                    </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    
    <?php $this->view('skek/V_footer'); ?>
</body>
</html>