<div id="wrapper">
    <nav id="sidebar">
        <div class="sidebar-header">
            <h4>Main Menu</h4>
        </div>
        <ul class="list-unstyled components">
            <li class="namabar">
                <a href="<?php echo base_url();?>dosen/index"><?php echo $this->session->userdata('nama');?></a>
            </li>
            <li>
                <a href="<?php echo base_url();?>dosen/index">Home</a>
            </li>
            <li class="active">
                <a href="<?php echo base_url();?>dosen/validasiview">Validasi Skek</a>
            </li>
            <li class="logoutbar">
                <a href="<?php echo base_url();?>login/logout">Log Out</a>
            </li>
        </ul>
    </nav>
    <div id="content">
    <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/index">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/validasiview">Validasi Skek</a></li>
					<li class="breadcrumb-item active" aria-current="page">Detail Skek</li>
				</ol>
			</nav>
            <h2 style="text-align: center;"><?php echo $this->session->userdata('nama'); ?></h2>
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" href="#rekapan">Rekapan</a></li>
            </ul>      
                <div class="tab-content">
                    <div id="rekapan" class="tab-pane fade in active">
					    <!-- <h3></h3> -->
                            <table class="table table-bordered table-striped dataTable no-footer" id ="tabelmhs" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Aspek Kegiatan</th>
                                        <th style="text-align:center;">Poin</th>                                        
                                        <th style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
										<td>ASPEK KEAGAAMAAN DAN MORAL PANCASILA</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspeka as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/aspekadsn/<?php echo $getdetilnim[0]['NIM']?>"> Detail</a>                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
										<td>ASPEK PENALARAN DAN IDEALISME</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekb as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/aspekbdsn/<?php echo $getdetilnim[0]['NIM']?>"> Detail</a>                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
										<td>ASPEK KEPEMIMPINAN DAN LOYALITAS</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekc as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/aspekcdsn/<?php echo $getdetilnim[0]['NIM']?>"> Detail</a>                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
										<td>ASPEK PEMENUHAN MINAT DAN BAKAT</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspekd as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/aspekddsn/<?php echo $getdetilnim[0]['NIM']?>"> Detail</a>                                               
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
										<td>ASPEK PENGABDIAN KEPADA MASYARAKAT</td>
                                        <td style="text-align:center;">
                                            <?php foreach($totalaspeke as $row): ?>
                                                <?php if($row['Total'] != null || $row['Total'] != ''){?>
                                                    <?php echo $row['Total'];?>
                                                <?php }else{echo "0";} ?>
                                            <?php endforeach; ?>
                                        </td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/aspekddsn/<?php echo $getdetilnim[0]['NIM']?>"> Detail</a>                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
						    <div style="padding:7px;"></div>
                            <div class = "col-md-2 kotak">
                                    <label for="total_skek" class="total">Total Skek</label>
                                    <input type="text" class="form-control text-right" id="total_skek" value="<?php echo $totalaallaspek[0]['Total'] ?>">
                                </div>
                    </div>
                </div>
    </div>
</div>

<?php $this->view('skek/V_footer'); ?>

</body>
</html>