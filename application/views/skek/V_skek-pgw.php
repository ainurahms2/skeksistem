<div id="wrapper">
        <!-- SIDEBAR HOLDER -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Main Menu</h4>
            </div>
            <ul class="list-unstyled components">
                <li class="namabar">
                    <a href="<?php echo base_url();?>pegawai/index"><?php echo $this->session->userdata('nama');?></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>pegawai/index">Home</a>    
                </li>
                <li class="active">
                    <a href="<?php echo base_url();?>pegawai/printskek">Print Skek</a>    
                </li>
                <li class="logoutbar">
                    <a href="<?php echo base_url();?>login/logout">Log Out</a>
                </li>
            </ul>
            <ul class="list-unstyled components">

            </ul>
        </nav>
        <div id="content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>pegawai/index">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Print Skek</li>
				</ol>
			</nav>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#printskek">Print Skek</a></li>
            </ul>
            <div class="form-group"> 
                <div class="tab-content">
                    <div id="printskek" class="tab-pane fade in active">
					    <h3>Print Skek Mahasiswa</h3>
						<div style="padding:5px;"></div>
                            <table class="table table-bordered table-striped dataTable no-footer" id ="tabelpgw" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Prodi</th>
                                        <th style="text-align:center;">Total Poin</th>
                                        <th style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $var = 1;
                                    foreach($showdata as $row)
                                    {
                                ?>
                                    <tr>
                                        <td><?php echo $var?></td>
                                        <td><?php echo $row->NAMA_MHS?></td>
                                        <td><?php echo $row->NIM?></td>
                                        <td><?php echo $row->PRODI_MHS?></td>
										<td style="text-align:center;"><?php echo $row->Total?></td>
										<td style="text-align:center;">
                                            <?php if($row->Total >= 60){?>
                                                <button type="button" onclick="" class="btn btn-success fas fa-print"><a href="<?php echo base_url();?>pegawai/printpdf/<?php echo $row->NIM?>"> Cetak Skek</a></button>
                                            <?php }else{?>
                                                <button type="button" onclick="" class="btn btn-success fas fa-print disabled" disabled> Cetak Skek</button>                                                
                                            <?php }?>
                                        </td>
                                    </tr>
                                <?php 
                                    $var++;
                                    }
                                ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php $this->view('skek/V_footer'); ?>

</body>
</html>