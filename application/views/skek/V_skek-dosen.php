<div id="wrapper">
        <!-- SIDEBAR HOLDER -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Main Menu</h4>
            </div>
            <ul class="list-unstyled components">
                <li class="namabar">
                    <a href="<?php echo base_url();?>dosen/index"><?php echo $this->session->userdata('nama');?></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>dosen/index">Home</a>                    
                </li>
                <li class="active">
                    <a href="<?php echo base_url();?>dosen/validasiview">Validasi Skek</a>
                </li>
                <li class="logoutbar">
                    <a href="<?php echo base_url();?>login/logout">Log Out</a>
                </li>
                
            </ul>
            <ul class="list-unstyled components">

            </ul>
        </nav>
        <div id="content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>dosen/index">Home</a></li>
					<li class="breadcrumb-item" aria-current="page">Validasi Skek</li>                    
				</ol>
			</nav>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#validasi">Validasi Skek</a></li>
                <li><a data-toggle="tab" href="#evaluasi">Evaluasi Skek Semua Mahasiswa</a></li>
            </ul>
            <div class="form-group"> 
                <div class="tab-content">
                    <div id="validasi" class="tab-pane fade in active">
					    <h3>Validasi Skek</h3>
						<div style="padding:5px;"></div>
                            <table class="table table-bordered table-striped dataTable no-footer" id ="tabelvalid" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width:1px;">No</th>
                                        <th>NIM</th>
                                        <th style="width:200px;">Nama</th>
                                        <th style="width:100px;">Aspek</th>
                                        <th>Sub-Aspek</th>
                                        <th style="width:50px;">Tingkat</th>
                                        <th>Posisi</th>
                                        <th>Poin</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($getallmhs->num_rows() > 0){
                                        $var=1;
                                        foreach($getallmhs->result() as $row)
                                        {   
                                    ?>
                                    <tr>
                                        <td><?php echo $var?></td>
                                        <td><?php echo $row->NIM?></td>
                                        <td><?php echo $row->NAMA_MHS?></td>
										<td><?php echo $row->ASPEK?></td>
										<td><?php echo $row->SUB_ASPEK?></td>
										<td><?php echo $row->TINGKAT?></td>
										<td><?php echo $row->POSISI?></td>
										<td><?php echo $row->POIN?></td>
										<td style="text-align:center;">
                                                <div>
                                                    <!-- <h2>contoh</h2> -->
                                                    
													<a target="_blank" href="<?php echo $row->FILE_UPLOAD?>" class="btn btn-info fas fa-search btnb buka-gambar"> Detail</a>
                                                    <button type="button" class="btn btn-success fas fa-check btnb" onclick="terimaaspek(<?php echo $row->ID_INPUT;?>)"> Terima</button>                                            
                                                    <button type="button" class="btn btn-danger fas fa-times btnb" onclick="tolakaspek(<?php echo $row->ID_INPUT;?>)"> Tolak</button>	
                                                    <button type="button" class="btn btn-warning fas fa-edit btnb" data-toggle="modal" data-target="#myModal"> Edit</button>
                                                    <div id="myModal" class="modal fade" role="dialog">
                                                        <form class="form-horizontal" method="POST" action="<?php echo base_url();?>dosen/updatedata">
                                                            <div class="modal-dialog">
                                                                <!-- Modal Content -->
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">Edit Skek Mahasiswa</h4>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="nim">NIM:</label>
                                                                            <div class="col-xs-8">
                                                                                <input class="form-control gapmodal" type="text" placeholder="NIM" id="nim" name="nim" value="<?php echo $row->NIM?>" required/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="nama">Nama:</label>
                                                                            <div class="col-xs-8">
                                                                                <input class="form-control gapmodal" type="text" placeholder="Nama" id="namas" name="namas" value="<?php echo $row->NAMA_MHS?>" required/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="aspek">Aspek:</label>
                                                                            <div class="col-xs-8">
                                                                                <select class="form-control gapmodal" id="aspek" name="aspek">
                                                                                    <option>Pilih Aspek</option>
                                                                                    <?php foreach($aspek as $row): ?>
                                                                                            <option value=<?php echo $row['ID_ASPEK'].">".$row['ASPEK'];?>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="subaspek">Sub Aspek:</label>
                                                                            <div class="col-xs-8">
                                                                                <select class="form-control gapmodal" id="subaspek" name="subaspek">
                                                                                    <option>Pilih Aspek terlebih dahulu</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="tingkat">Tingkat:</label>
                                                                            <div class="col-xs-8">
                                                                                <select class="form-control gapmodal" id="tingkat" name="tingkat">
                                                                                    <option>Pilih Sub-Aspek terlebih dahulu</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" for="posisi">Posisi:</label>
                                                                            <div class="col-xs-8">
                                                                                <select class="form-control gapmodal" id="posisi" name="posisi">
                                                                                    <option>Pilih Tingkatan terlebih dahulu</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label style="text-align:left;" class="control-label col-xs-3" >Poin:</label>
                                                                            <div class="col-xs-8">
                                                                                <input class="form-control gapmodal" type="text" placeholder="Poin" id="poin" name="poin" required/>
                                                                                <input type="hidden" id="idrubrikhid" name="idrubrikhid">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer gapmodal">
                                                                        <button class="btn btn-info">Simpan</button>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>                                 
                                        </td>
                                    </tr>
                                    <?php 
                                    $var++;
                                        }
                                    }else{
                                    ?>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                    </div>
                    <div id="evaluasi" class="tab-pane fade">
					    <h3>Evaluasi Skek Semua Mahasiswa</h3>
						<div style="padding:5px;"></div>
                            <table class="table table-bordered table-striped dataTable no-footer" id ="tabeleval" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th style="text-align:center;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if($getevalmhs->num_rows() > 0){
                                    $var=1; 
                                    foreach($getevalmhs->result() as $row)
                                    {
                                ?>
                                    <tr>
                                        <td><?php echo $var?></td>
										<td><?php echo $row->NAMA_MHS?></td>
										<td><?php echo $row->NIM?></td>
										<td style="text-align:center;">
                                            <button type="button" class="btn btn-success fas fa-search"><a href="<?php echo base_url();?>dosen/permhs/<?php echo $row->NIM?>"> Lihat Detail</a>                                            
                                        </td>
                                    </tr>
                                    <?php 
                                    $var++;
                                    }
                                    }else{
                                    ?>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php $this->view('skek/V_footer'); ?>
<script type="text/javascript">
    function terimaaspek(ID_INPUT){
        if (confirm("Anda akan menerima skek ini?")) {
        $.ajax({
            url: "<?php echo base_url();?>dosen/terimaaspek",
            type: 'post',
            data: {id: ID_INPUT},
            success: function () {
                alert('Anda telah memvalidasi skek ini');
                location.reload();
            },
            error: function() {
                alert('ajax failure');
            }
        });
    } else {
        alert(id + " not deleted");
    }
    }
    function tolakaspek(ID_INPUT){
        if (confirm("Anda akan menolak skek ini?")) {
        $.ajax({
            url: "<?php echo base_url();?>dosen/tolakaspek",
            type: 'post',
            data: {id: ID_INPUT},
            success: function () {
                alert('Anda telah menolak skek ini');
                location.reload();
            },
            error: function() {
                alert('ajax failure');
            }
        });
    } else {
        alert(id + " not deleted");
    }
    }
    $(document).ready(function(){
        var aspek = "";
        var subaspek = "";
        var tingkatan = "";
        $('#aspek').change(function(){
            var aspek=$('#aspek').val();
            $.ajax({
                url : "<?php echo base_url();?>mahasiswa/getsubaspek",
                method : "POST",
                data : {id_aspek: aspek},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    html += '<option>Pilih Sub-Aspek</option>';
                    for(i=0; i<data.length; i++){
                        html += '<option value='+data[i].ID_SUB_ASPEK+'>'+data[i].SUB_ASPEK+'</option>';
                    }
                    $('#subaspek').html(html);
                }
            });
        });
        $('#subaspek').change(function(){
            var subaspek=$('#subaspek').val();
            $.ajax({
                url : "<?php echo base_url();?>mahasiswa/gettingkat",
                method : "POST",
                data : {id_sub_aspek: subaspek},
                async : false,
                dataType : 'json',
                success: function(data){
                    var html = '';
                    var i;
                    html += '<option>Pilih Tingkat</option>';
                    for(i=0; i<data.length; i++){
                        html += '<option value=' + data[i].ID_TINGKAT + '>' + data[i].TINGKAT + '</option>';
                    }
                    $('#tingkat').html(html);
                }
            });
        });
            $('#tingkat').change(function(){
                var aspek = $('#aspek').val();
                var subaspek = $('#subaspek').val();
                var tingkat=$('#tingkat').val();
                $.ajax({
                    url : "<?php echo base_url();?>mahasiswa/getposisi",
                    method : "POST",
                    data : {id_aspek: aspek,id_sub_aspek: subaspek, id_tingkat: tingkat},
                    async : false,
                    dataType : 'json',
                    success: function(data){
                        var html = "";
                        var i;
                        html += '<option>Pilih Posisi</option>';
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].ID_POSISI+'>'+data[i].POSISI+'</option>';
                        }
                        $('#posisi').html(html);
                    }
                });
            });
            $('#posisi').change(function(){
                var aspek = $('#aspek').val();
                var subaspek = $('#subaspek').val();
                var tingkat=$('#tingkat').val();
                var posisi=$('#posisi').val();
                var id_rubrik;
                $.ajax({
                    url : "<?php echo base_url();?>mahasiswa/getpoin",
                    method : "POST",
                    data : {id_aspek: aspek,id_sub_aspek: subaspek, id_tingkat: tingkat, id_posisi: posisi, id_rubrik: id_rubrik},
                    async : false,
                    dataType : 'json',
                    success: function(data){
                        $("#poin").attr("placeholder",(data[0].POIN));
                        $("#idrubrikhid").attr("value",(data[0].ID_RUBRIK));                        
                    }
                });
            });
    });
    // $(document).on("click", ".buka-gambar", function () {
    //     var data = $(this).data('id');
    //     $("#pic-berkas").attr("src", data);
    // });
</script>
</body>
</html>