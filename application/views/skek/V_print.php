<!DOCTYPE >
<html>
    <head>
        <title>SKEK : SISTEM KREDIT EKSTRAKURIKULER
        </title>

        <style>

            .HeaderBG {
                color: black;
                font-weight: bold;
                background-color: white;
            }

            .tab_header {
                border-bottom: 1px solid black;
                margin-bottom: 5px;
            }
            .div_headeritem {
                float: left;
            }
            .div_header,
            .div_preheader {
                font-family: "Times New Roman";
            }
            .div_preheader {
                font-weight: bold;
            }
            .div_header {
                font-size: 12px;
            }
            .div_headertext {
                font-size: 13px;
                font-style: italic;
            }
        </style>

    </head>
    <div align="center">
        <body
            leftmargin="0"
            rightmargin="0"
            topmargin="0"
            bottommargin="0"
            onload="window.print();">
            <br>
            <table class="tab_header" width="700">
                <thead>
                    <tr>
                        <td width="70" align="center">
                            <img src="<?php echo base_url('./assets/img/iain.gif'); ?>" width="75">
                        </td>
                        <td valign="top" colspan="5">
                            <div class="div_preheader">KEMENTERIAN AGAMA</div>
                            <div class="div_header">UNIVERSITAS ISLAM NEGERI SUNAN AMPEL - SURABAYA</div>
                            <div class="div_headertext">Jl. Ahmad Yani 117 Surabaya. Telp. 031-8410298 Fax.
                                031-8413300. website: www.sunan-ampel.ac.id, email:info@sunan-ampel.ac.id</div>
                        </td>
                    </tr>
                </thead>
            </table>
            <table width="650">
                <tr>
                    <td align="center">
                        <font size="2">
                            <b>SISTEM KREDIT EKSTRAKURIKULER (SKEK)</b>
                        </font>
                    </td>
                </tr>
            </table>
            <table width="600" cellpadding="0">
    
                <tr>
                    <td nowrap="nowrap">
                        <font size="2">
                            <strong>NIM</strong>
                        </font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2"><?php echo ": ".$getmhs[0]->NIM?>
                        </font>
                    </td>
                    <td>
                        <font size="-1">&nbsp;</font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2">
                            <strong>Jurusan</strong>
                        </font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2"><?php echo ": ".$getmhs[0]->PRODI_MHS?></font>
                    </td>
                </tr>
                <br>
                <br>
                <tr>
                    <td nowrap="nowrap">
                        <font size="2">
                            <strong>Nama</strong>
                        </font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2"><?php echo ": ".$getmhs[0]->NAMA_MHS?></font>
                    </td>
                    <td>
                        <font size="-1">&nbsp;</font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2">
                            <strong>Dosen Wali</strong>
                        </font>
                    </td>
                    <td nowrap="nowrap">
                        <font size="2"><?php echo ": ".$dosen[0]->nama?></font>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table
                width="600"
                border="1"
                bordercolor="black"
                cellspacing="0"
                cellpadding="1"
                style="border-collapse:collapse;">
            
                <tr align="center" bgcolor="#330099" height="10">
                    <td class="HeaderBG">
                        <font size="-1">No.</font>
                    </td>
                    <td class="HeaderBG">
                        <font size="-1">Aspek
                            <br>Kegiatan</font>
                    </td>
                    <td class="HeaderBG">
                        <font size="-1">Level
                            <br>(Tingkat)</font>
                    </td>
                    <td class="HeaderBG">
                        <font size="-1">Posisi
                            <br>(Sebagai)</font>
                    </td>
                    <td class="HeaderBG">
                        <font size="-1">Nilai
                            <br>Kredit (SKEK)</font>
                    </td>

                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" style="text-align:center;">
                        <b>Aspek Ke-Islaman dan Moral Pancasila</b>
                    </td>
                    <td></td>
                </tr>
                <?php 
                $var = 1;
                foreach($getmhsa as $row){
                ?>
                <tr valign="top" class="NormalBG">
                    <td><?php echo $var?></td>
                    <td><?php echo $row->SUB_ASPEK?></td>
                    <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                    <td style="text-align:center;"><?php echo $row->POSISI?></td>
                    <td style="text-align:center;"><?php echo $row->POIN?></td>
                <?php 
                $var++;
                }
                ?>
                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" style="text-align:center;">
                        <b>Aspek Penalaran dan Idealisme</b>
                    </td>
                    <td></td>
                </tr>
                <?php 
                $var = 1;
                foreach($getmhsb as $row){
                ?>
                <tr valign="top" class="NormalBG">
                    <td><?php echo $var?></td>
                    <td><?php echo $row->SUB_ASPEK?></td>
                    <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                    <td style="text-align:center;"><?php echo $row->POSISI?></td>
                    <td style="text-align:center;"><?php echo $row->POIN?></td>
                <?php 
                $var++;
                }
                ?>
                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" style="text-align:center;">
                        <b>Aspek Kepemimpinan dan Loyalitas</b>
                    </td>
                    <td></td>
                </tr>
                <?php 
                $var = 1;
                foreach($getmhsc as $row){
                ?>
                <tr valign="top" class="NormalBG">
                    <td><?php echo $var?></td>
                    <td><?php echo $row->SUB_ASPEK?></td>
                    <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                    <td style="text-align:center;"><?php echo $row->POSISI?></td>
                    <td style="text-align:center;"><?php echo $row->POIN?></td>
                <?php 
                $var++;
                }
                ?>
                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" style="text-align:center;">
                        <b>Aspek Pemenuhan Bakat dan Minat</b>
                    </td>
                    <td></td>
                </tr>
                <?php 
                $var = 1;
                foreach($getmhsd as $row){
                ?>
                <tr valign="top" class="NormalBG">
                    <td><?php echo $var?></td>
                    <td><?php echo $row->SUB_ASPEK?></td>
                    <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                    <td style="text-align:center;"><?php echo $row->POSISI?></td>
                    <td style="text-align:center;"><?php echo $row->POIN?></td>
                <?php 
                $var++;
                }
                ?>
                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" style="text-align:center;">
                        <b>Aspek Pengabdian Kepada Masyarakat</b>
                    </td>
                    <td></td>
                </tr>
                <?php 
                $var = 1;
                foreach($getmhse as $row){
                ?>
                <tr valign="top" class="NormalBG">
                    <td><?php echo $var?></td>
                    <td><?php echo $row->SUB_ASPEK?></td>
                    <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                    <td style="text-align:center;"><?php echo $row->POSISI?></td>
                    <td style="text-align:center;"><?php echo $row->POIN?></td>
                <?php 
                $var++;
                }
                ?>
                </tr>
                <tr valign="top" class="AlternateBG">
                    <td colspan="4" align="center">
                        <b>Jumlah</b>
                    </td>
                    <td style="text-align:center;">
                    <?php echo $gettotal[0]['Total'] ?>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table width="650" border="0" cellspacing="0" cellpadding="0">
                <tr height="30" valign="middle">
                    <td width="335" align="left">
                        <font size="-1">&nbsp;</font>
                    </td>
                    <td width="315" align="left">
                        <font size="-1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Surabaya,
                                <u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp;
                                </u>
                            </b>
                        </font>
                    </td>
                </tr>
                <br>
                <tr height="40" valign="middle">
                    <td width="335" align="left">
                        <font size="-1">&nbsp;</font>
                    </td>
                    <td width="315" align="left">
                        <font size="-1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>Dosen Wali.
                                <br><br><br><br><br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<u>&nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</u>
                            </b>
                        </font>
                    </td>
                </tr>
            </table>
        </body>
    </div>
</html>