<div id="wrapper">
        <!-- SIDEBAR HOLDER -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h4>Main Menu</h4>
            </div>
            <ul class="list-unstyled components">
                <li class="namabar">
                    <a href="<?php echo base_url();?>mahasiswa/index"><?php echo $this->session->userdata('nama');?></a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>mahasiswa/index">Home</a>
                </li>
                <li class="active">
                    <a href="<?php echo base_url();?>mahasiswa/inputview">Masukkan Data Skek</a>
                </li>
                <li class="logoutbar">
                    <a href="<?php echo base_url();?>login/logout">Log Out</a>
                </li>
            </ul>
        </nav>
        <div id="content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>mahasiswa/index">Home</a></li>
			        <li class="breadcrumb-item"><a href="<?php echo base_url();?>mahasiswa/inputview">Masukkan Data Skek</a></li>
					<li class="breadcrumb-item active" aria-current="page">Detail Skek</li>
				</ol>
			</nav>
            <h2 style="text-align: center;"><?php echo $this->session->userdata('nama'); ?></h2>
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" href="#detailskeka">Detail Skek</a></li>
            </ul>
                <div class="form-group">      
                    <div class="tab-content">
                        <div id="detailskeka" class="tab-pane fade in active">
                            <h3>ASPEK PEMENUHAN MINAT DAN BAKAT</h3>
                            <div style="padding:5px;"></div>
                                <table class="table table-bordered table-striped dataTable no-footer" id ="tabelmhs" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:1px;">No</th>
                                            <th style="width:250px;">Sub-Aspek</th>
                                            <th style="width:200px; text-align:center;">Tingkatan</th>
                                            <th style="width:5px; text-align:center;">Posisi</th>
                                            <th style="width:5px; text-align:center;">Poin</th>
                                            <th style="width:100px; text-align:center;">Status</th>
                                            <th style="width:150px; text-align:center;">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(($getaspekd->num_rows() > 0)){ 
                                        $var=1;
                                        foreach($getaspekd->result() as $row)
                                        {
                                    ?>
                                        <tr>
                                            <td style="text-align:center;"><?php echo $var?></td>
                                            <td><?php echo $row->SUB_ASPEK?></td>
                                            <td style="text-align:center;"><?php echo $row->TINGKAT?></td>
                                            <td style="text-align:center;"><?php echo $row->POSISI?></td>
                                            <td style="text-align:center;"><?php echo $row->POIN?></td>
                                            <td style="text-align:center;">
                                                <?php if($row->STATUS == 0){?>
                                                    <span class="label label-warning">
                                                        MENUNGGU
                                                    </span>
                                                <?php }else if($row->STATUS == 1){?>
                                                    <span class="label label-success">
                                                        DITERIMA
                                                    </span>
                                                <?php }else{?>
                                                    <span class="label label-danger">
                                                        DITOLAK
                                                    </span>
                                                <?php }?>    
                                            </td>
                                            <td>
                                                <div>
                                                    <!-- <h2>contoh</h2> -->
                                                    <a target="_blank" href="<?php echo $row->FILE_UPLOAD?>" class="btn btn-success fas fa-search btna buka-gambar"> Detail</a>
                                                    <button type="button" class="btn btn-danger fas fa-trash btna" onclick="deleteaspek(<?php echo $row->ID_INPUT;?>)"> Delete</button>                                       
                                                    <div class="modal fade" id="myModal" role="dialog">
                                                        <div class="modal-dialog" data-id="">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                    <h4 class="modal-title">Gambar Sertifikat</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                        
                                                                    <img id="pic-berkas" class="card-img-top img-responsive" src="" alt="Card image cap" style="width:842px; height:595px;">
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>     
                                            </td>
                                        </tr>
                                    <?php 
                                    $var++;
                                        }
                                    }else{
                                    ?>
                                    <?php 
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <div style="padding:7px;"></div>
                                    <div class = "col-md-2 kotak">
                                        <label for="total_skek" class="total">Total Skek</label>
                                        <input type="text" class="form-control text-right" id="total_skek" value="<?php echo $totalaspekd[0]['Total'] ?>">
                                    </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    
    <?php $this->view('skek/V_footer'); ?>
<script type="text/javascript">
    function deleteaspek(ID_ASPEK){
        if (confirm("Are you sure?")) {
        $.ajax({
            url: "<?php echo base_url();?>mahasiswa/deleteaspek",
            type: 'post',
            data: {id: ID_ASPEK},
            success: function () {
                alert('Data berhasil didelete');
                location.reload();
            },
            error: function () {
                alert('ajax failure');
            }
        });
    } else {
        alert(id + " not deleted");
    }
    }
    $(document).on("click", ".buka-gambar", function () {
        var data = $(this).data('id');
        $("#pic-berkas").attr("src", data);
    });
</script>
</body>
</html>