<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Login extends CI_Controller {
        function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            $this->load->model('M_login'); 
			$this->load->helper(array('form', 'url', 'file','download','cookie'));			
        }
        public function index()
        {
            // $this->load->view('skek/V_login');
			redirect("http://ctrl.uinsby.ac.id");
        }
		public function sso(){
			$par = get_cookie('nip');
			$token = get_cookie('token');
			$param['par'] = $par;
			$param['token'] = $token;
			$this->load->view('sso.php',$param);
		}
		public function login1(){
                $username = $this->input->post('username');
                $token    = $this->input->post('token');
                if($token == get_cookie('token')){
                        $par    = base64_decode(base64_decode($username));
                        $datax  = explode("|",$par);
                        $nip    = $datax[0];
                        $tgl    = $datax[1];
                        if($tgl == date("Y-m-d")){
                                $this->proseslogin2($nip);
                                //kalau user ketemu
                               /* if($user != null){
                                        //renew cookie
                                        $this->renew_session_cookie($user);
                                        redirect('home');
                                }else{
                                        echo"<script>window.close()</script>";
                                }*/
                        }else{
                                echo"<script>window.close()</script>";
                        }
                }else{
                        echo"<script>window.close()</script>";
                }
        }
		 public function proseslogin2($nip){
            
            $count = $this->M_login->ceklogin2($nip);
            if($count >= 1)
            {
                $getdata = $this->M_login->getdata($nip);
                $user = $this->M_login->getuser($getdata[0]->id_pegawai);
                $this->session->set_userdata($user[0]);
                $pengelola = $this->M_login->pengelola();
                // print_r($pengelola);
                foreach($pengelola as $row){
                    if ($nip == $row->nip) {
                        redirect(base_url("pegawai/index"));
                        // echo $row->nip;
                    }
                }
                    if($user[0]['id_tipe'] == "2"){
                        redirect(base_url("dosen/index"));
                    }else{
                        echo "Tidak punya akses";
                    }
            }elseif($count=='0'){
                //$data = file_get_contents('http://eis.uinsby.ac.id/eis/login/'.$username.'/'.$pass);
				$data = file_get_contents('http://eis.uinsby.ac.id/eis/login/'.$nip);
                $obj = json_decode($data, true);
                if(!empty($obj[0]))
                {
                    $this->session->set_userdata($obj[0]);
                    redirect(base_url("mahasiswa/index"));
                }
                else{
                    // redirect(base_url());
                    echo "gagal";
                }
            }else{
                // redirect(base_url());                
                echo "gagal";
            }
        }
        public function proseslogin(){
            $username = $this->input->post('username');
            $pass=$this->input->post('password');
            $password = md5($this->input->post('password'));
            $count = $this->M_login->ceklogin($username,$password);
            if($count == 1)
            {
                $getdata = $this->M_login->getdata($username);
                $user = $this->M_login->getuser($getdata[0]->id_pegawai);
                $this->session->set_userdata($user[0]);
                $pengelola = $this->M_login->pengelola();
                // print_r($pengelola);
                foreach($pengelola as $row){
                    if ($username == $row->nip) {
                        redirect(base_url("pegawai/index"));
                        // echo $row->nip;
                    }
                }
                    if($user[0]['id_tipe'] == "2"){
                        redirect(base_url("dosen/index"));
                    }else{
                        echo "Tidak punya akses";
                    }
            }elseif($count=='0'){
                $data = file_get_contents('http://eis.uinsby.ac.id/eis/login/'.$username.'/'.$pass);
                $obj = json_decode($data, true);
                if(!empty($obj[0]))
                {
                    $this->session->set_userdata($obj[0]);
                    redirect(base_url("mahasiswa/index"));
                }
                else{
                    // redirect(base_url());
                    echo "gagal";
                }
            }else{
                // redirect(base_url());                
                echo "gagal";
            }
        }
        public function logout()
        {
            $this->load->view('skek/V_login');
            $this->session->sess_destroy();            
        }
    }
?>