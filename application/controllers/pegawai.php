<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Pegawai extends CI_Controller {
        function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            // $this->load->library('../controllers/login');
            $this->load->model('M_pegawai');
            $this->load->model('M_login');
            
        }
        public function index()
        {

            // $namacek = $this->session->userdata('nama');
            $nip = $this->M_login->ceklogin2($nip);
            if($nip != null){
                $this->load->view('skek/V_header-pgw');
                $this->load->view('skek/V_home-pgw');            
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function printskek()
        {
            // print_r($data);
            // print($data);
            $nip = $this->M_login->ceklogin2($nip);
            if($nip != null){
                $data["showdata"] = $this->M_pegawai->showdata();
                $this->load->view('skek/V_header-pgw');
                $this->load->view('skek/V_skek-pgw',$data);
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function permhs()
        {
            $nip = $this->M_login->ceklogin2($nip);
            if($nip != null){
                $this->load->view('skek/V_header-pgw');
                $this->load->view('skek/V_skek-permhs');
            }else{
                redirect(base_url());
            }         
            // $this->load->view('skek/V_footer');
        }
        public function printpdf($nim)
        {
            $nip = $this->M_login->ceklogin2($nip);
            if($nip != null){
                $data['showdata'] = $this->M_pegawai->showdata();
                $data['dosen'] = $this->M_pegawai->getdosen($data['showdata'][0]->NIP);
                $data['getmhs'] = $this->M_pegawai->getmhs($nim);
                $data['getmhsa'] = $this->M_pegawai->getmhsa($nim);
                $data['getmhsb'] = $this->M_pegawai->getmhsb($nim);
                $data['getmhsc'] = $this->M_pegawai->getmhsc($nim);
                $data['getmhsd'] = $this->M_pegawai->getmhsd($nim);
                $data['getmhse'] = $this->M_pegawai->getmhse($nim);
                $data['gettotal'] = $this->M_pegawai->gettotal($nim);
                $this->load->view('skek/V_print',$data);
            }else{
                redirect(base_url());
            }
            // print_r($data['getmhsa']);
        }
    }
?>