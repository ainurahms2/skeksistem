<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Dosen extends CI_Controller {
        function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            $this->load->model('M_dosen');
            $this->load->model('M_inputskek');

            // $this->load->library('../controllers/login');
            
        }
        public function index()
        {
            // $namacek = $this->session->userdata('nama');
            $iddos = $this->session->userdata('nip');
            // $login = $this->M_login->getuser();
            if($iddos!=null)
            {
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_home-dosen');            

            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function validasiview()
        {
            $iddos = $this->session->userdata('nip');
            if($iddos!=null){
                $data['aspek'] = $this->M_dosen->getaspek();            
                $data["getallmhs"] = $this->M_dosen->getallmhs();
                $data["getevalmhs"] = $this->M_dosen->getevalmhs();
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_skek-dosen',$data);
            }else{
                redirect(base_url());
            }
            // print_r($data);
            // $this->load->view('skek/V_footer');
        }
        public function permhs($nim)
        {
            // $data['nama'] = $this->M_dosen->getnamamhs();
            $iddos = $this->session->userdata('nip');
            if($iddos!=null){
                $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
                $data["totalaspeka"] = $this->M_dosen->getotalaspeka($nim);
                $data["totalaspekb"] = $this->M_dosen->getotalaspekb($nim);
                $data["totalaspekc"] = $this->M_dosen->getotalaspekc($nim);
                $data["totalaspekd"] = $this->M_dosen->getotalaspekd($nim);
                $data["totalaspeke"] = $this->M_dosen->getotalaspeke($nim);
                $data["totalaallaspek"] = $this->M_dosen->getotalallaspek($nim);
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_skek-permhs',$data);
            }else{
                redirect(base_url());
            }    

            // print_r($data);
            // print_r($data);
            // $data['getmhs'] = $this->M_dosen->getaspeka();
            // $this->load->view('skek/V_footer');
        }
        public function aspekadsn($nim)
        {
            $iddos = $this->session->userdata('nip');

            if($iddos!=null){
                $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
                $data['getaspeka'] = $this->M_dosen->getaspeka($nim);
                $data["totalaspeka"] = $this->M_dosen->getotalaspeka($nim);
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_aspeka-dsn',$data);
            }else{
                redirect(base_url());
            }
        }
        public function aspekbdsn($nim)
        {
            $iddos = $this->session->userdata('nip');
            if($iddos!=null){
                $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
                $data['getaspekb'] = $this->M_dosen->getaspekb($nim);
                $data["totalaspekb"] = $this->M_dosen->getotalaspekb($nim);
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_aspekb-dsn',$data);
            }else{
                redirect(base_url());
            }         
        }
        public function aspekcdsn($nim)
        {
            $iddos = $this->session->userdata('nip');
            if($iddos!=null){
                $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
                $data['getaspekc'] = $this->M_dosen->getaspekc($nim);
                $data["totalaspekc"] = $this->M_dosen->getotalaspekc($nim);
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_aspekc-dsn',$data);
            }else{
                redirect(base_url());
            }
        }
        public function aspekddsn($nim)
        {
            $iddos = $this->session->userdata('nip');
            if($iddos!=null){
                $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
                $data['getaspekd'] = $this->M_dosen->getaspekd($nim);
                $data["totalaspekd"] = $this->M_dosen->getotalaspekd($nim);
                $this->load->view('skek/V_header-dsn');
                $this->load->view('skek/V_aspekd-dsn',$data);
            }else{
                redirect(base_url());
            }
        }
        public function aspekdesn($nim)
        {
            $data['getdetilnim'] = $this->M_dosen->getdetilnim($nim);
            $data['getaspeke'] = $this->M_dosen->getaspeke($nim);
            $data["totalaspeke"] = $this->M_dosen->getotalaspeke($nim);
            $this->load->view('skek/V_header-dsn');
            $this->load->view('skek/V_aspeke-dsn',$data);
        }
        public function terimaaspek()
        {
            $id = $this->input->post('id');
            $this->M_dosen->terimaaspek($id);
        }
        public function tolakaspek()
        {
            $id = $this->input->post('id');
            $this->M_dosen->tolakaspek($id);
        }
        public function updatedata()
        {
            $this->M_dosen->updatedata($nim,$nama);
            redirect('dosen/validasiview');
        }
    }
?>