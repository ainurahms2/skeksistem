<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Mahasiswa extends CI_Controller {
        
        function __construct()
        {
            parent::__construct();
            $this->load->library('session');
            $this->load->model('M_inputskek');
            $this->load->helper('form','url');
            
            
        }
        public function index()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');
            if($namacek!=null && $nimcek!=null)
            {
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_home-mhs');            
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function inputview()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');
            
            if($namacek!=null && $nimcek!=null){
                $data['aspek'] = $this->M_inputskek->getaspek();
                $data["totalaspeka"] = $this->M_inputskek->getotalaspeka();
                $data["totalaspekb"] = $this->M_inputskek->getotalaspekb();
                $data["totalaspekc"] = $this->M_inputskek->getotalaspekc();
                $data["totalaspekd"] = $this->M_inputskek->getotalaspekd();
                $data["totalaspeke"] = $this->M_inputskek->getotalaspeke();
                $data["totalaallaspek"] = $this->M_inputskek->getotalallaspek();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_skek-mhs',$data);

            }else{
                redirect(base_url());
            }
            
        }
        public function detilaspeka()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');

            if($namacek!=null && $nimcek!=null){
                $data["getaspeka"] = $this->M_inputskek->getaspeka();
                $data["totalaspeka"] = $this->M_inputskek->getotalaspeka();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_aspeka',$data);
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer',$data);
        }
        public function detilaspekb()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');
            if($namacek!=null && $nimcek!=null){
                $data["getaspekb"] = $this->M_inputskek->getaspekb();
                $data["totalaspekb"] = $this->M_inputskek->getotalaspekb();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_aspekb',$data);
            }else{
                redirect(base_url());

            }
            // $this->load->view('skek/V_footer');
        }
        public function detilaspekc()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');

            if($namacek!=null && $nimcek!=null){
                $data["getaspekc"] = $this->M_inputskek->getaspekc();
                $data["totalaspekc"] = $this->M_inputskek->getotalaspekc();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_aspekc',$data);
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function detilaspekd()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');
            if($namacek!=null && $nimcek!=null){
                $data["getaspekd"] = $this->M_inputskek->getaspekd();
                $data["totalaspekd"] = $this->M_inputskek->getotalaspekd();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_aspekd',$data);
            }else{
                redirect(base_url());
            }
            // $this->load->view('skek/V_footer');
        }
        public function detilaspeke()
        {
            $namacek = $this->session->userdata('nama');
            $nimcek = $this->session->userdata('nim');
            if($namacek!=null && $nimcek!=null){
                $data["getaspeke"] = $this->M_inputskek->getaspeke();
                $data["totalaspeke"] = $this->M_inputskek->getotalaspeke();
                $this->load->view('skek/V_header-mhs');
                $this->load->view('skek/V_aspeke',$data);
            }else{
                redirect(base_url());
                
            }
            // $this->load->view('skek/V_footer');
        }
        public function getsubaspek()
        {
            
            $id_aspek = $this->input->post('id_aspek');
            $data = $this->M_inputskek->getsubaspek($id_aspek);
            echo json_encode($data);
            // echo $data;
            // print_r($getdata);
        }
        public function gettingkat()
        {
            $id_sub_aspek = $this->input->post('id_sub_aspek');
            $data = $this->M_inputskek->gettingkat($id_sub_aspek);
            echo json_encode($data);
        }
        public function getposisi()
        {
            // $id = $this->input->post('id');
            $idaspek = $this->input->post('id_aspek');
            $idsubaspek = $this->input->post('id_sub_aspek');
            $idtingkat = $this->input->post('id_tingkat');
            $data = $this->M_inputskek->getposisi($idaspek,$idsubaspek,$idtingkat);
            // print_r($data);
            echo json_encode($data);
        }
        public function getpoin()
        {
            //belum done
            // $id = $this->input->post('id');
            $idaspek = $this->input->post('id_aspek');
            $idsubaspek = $this->input->post('id_sub_aspek');
            $idtingkat = $this->input->post('id_tingkat');
            $idposisi = $this->input->post('id_posisi');
            $data = $this->M_inputskek->getpoin($idaspek,$idsubaspek,$idtingkat,$idposisi);
            // print_r($data);
            echo json_encode($data);
        }
        // public function savedatamhs()
        // {
            
        //     $data = $this->M_inputskek->simpandatamhs();
        //     echo json_encode($data);
        //     redirect("/mahasiswa/inputview");
        // }

        public function simpandatamhs(){
            $nim = $this->session->userdata('nim');
			$result= $this->M_inputskek->simpandatamhs();
            redirect("mahasiswa/inputview");
            // Input with gambar
            // $files = $_FILES;
            // // print_r($_FILES['sertif']);
            // $datainfo = array();        
            // if ($_FILES['sertif']['error']==0) {
                // // './uploads/sertif';
                    // mkdir('./uploads/sertif'.$nim, 0777, TRUE);
                    // // $config['upload_path'] = './uploads/sertif'. $nim;
                     // $config['upload_path'] = './uploads';
                    // $config['allowed_types'] = 'jpg|png|jpeg|pdf';
                    // $config['max_size']  = '2048';
                    // $config['remove_space'] = TRUE;
                    // $this->load->library('upload', $config); 
                    
                    // $_FILES['sertif']['name']= $files['sertif']['name'];
                    // $_FILES['sertif']['type']= $files['sertif']['type'];
                    // $_FILES['sertif']['tmp_name']= $files['sertif']['tmp_name'];
                    // $_FILES['sertif']['error']= $files['sertif']['error'];
                    // $_FILES['sertif']['size']= $files['sertif']['size'];
                    // $this->upload->do_upload("sertif");
                    // $datainfo[] = $this->upload->data();
                    // $dir=str_replace("C:/xampp/htdocs/skeksistem/",base_url(),$this->upload->data('full_path'));

                    // // print_r($datainfo);
                    // $result = $this->M_inputskek->simpandatamhs($datainfo,$dir);
                    // redirect("mahasiswa/inputview");
                    // // print_r($_FILES['sertif']['error']);
            // } else {
                // // $result= $this->M_inputskek->simpandatamhs($nim);
                // echo "tidakupload";
            // }
    
            // // $data = $this->M_inputskek->simpandatamhs($upload);
            // // if($data == 1){
            // //     redirect("mahasiswa/inputview");
            // // }else{
            // //     echo "error";
            // // }
            // End of input with gambar
        }
        public function deleteaspek(){
            $id = $this->input->post('id');
            $this->M_inputskek->deleteaspek($id);
        }
    }
?>