<?php 
    class M_pegawai extends CI_model{
        function __construct()
        {
            parent::__construct();
            $this->db = $this->load->database('default', TRUE);
            $this->db2  = $this->load->database('simpeg', TRUE);
        }
        function showdata()
        {
            $sql = "SELECT b.NAMA_MHS,b.NIM,b.PRODI_MHS, b.NIP, SUM(a.POIN) AS Total
            FROM tabel_rubrik a
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.STATUS = 1 GROUP BY b.NIM";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getmhs($nim)
        {
            $sql = "SELECT b.NAMA_MHS,b.NIM,b.PRODI_MHS, b.NIP, SUM(a.POIN) AS Total
            FROM tabel_rubrik a
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE NIM = '$nim'";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getdosen($nip)
        {
            
            $sql = "SELECT * FROM tbpegawai WHERE nip = '$nip'";
            $query = $this->db2->query($sql);
            return $query->result();
        }
        function getmhsa($nim){
            $sql = "SELECT * FROM input_mhs im
            JOIN tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK
            JOIN tabel_aspek ta ON tr.ID_ASPEK = ta.ID_ASPEK
            JOIN tabel_sub_aspek tsa ON tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK
            JOIN tabel_tingkat tt ON tr.ID_TINGKAT = tt.ID_TINGKAT
            JOIN tabel_posisi tp ON tr.ID_POSISI = tp.ID_POSISI
            WHERE NIM = '$nim' AND
            tr.ID_ASPEK = 1";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getmhsb($nim){
            $sql = "SELECT * FROM input_mhs im
            JOIN tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK
            JOIN tabel_aspek ta ON tr.ID_ASPEK = ta.ID_ASPEK
            JOIN tabel_sub_aspek tsa ON tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK
            JOIN tabel_tingkat tt ON tr.ID_TINGKAT = tt.ID_TINGKAT
            JOIN tabel_posisi tp ON tr.ID_POSISI = tp.ID_POSISI
            WHERE NIM = '$nim' AND
            tr.ID_ASPEK = 2";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getmhsc($nim){
            $sql = "SELECT * FROM input_mhs im
            JOIN tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK
            JOIN tabel_aspek ta ON tr.ID_ASPEK = ta.ID_ASPEK
            JOIN tabel_sub_aspek tsa ON tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK
            JOIN tabel_tingkat tt ON tr.ID_TINGKAT = tt.ID_TINGKAT
            JOIN tabel_posisi tp ON tr.ID_POSISI = tp.ID_POSISI
            WHERE NIM = '$nim' AND
            tr.ID_ASPEK = 3";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getmhsd($nim){
            $sql = "SELECT * FROM input_mhs im
            JOIN tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK
            JOIN tabel_aspek ta ON tr.ID_ASPEK = ta.ID_ASPEK
            JOIN tabel_sub_aspek tsa ON tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK
            JOIN tabel_tingkat tt ON tr.ID_TINGKAT = tt.ID_TINGKAT
            JOIN tabel_posisi tp ON tr.ID_POSISI = tp.ID_POSISI
            WHERE NIM = '$nim' AND
            tr.ID_ASPEK = 4";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function getmhse($nim){
            $sql = "SELECT * FROM input_mhs im
            JOIN tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK
            JOIN tabel_aspek ta ON tr.ID_ASPEK = ta.ID_ASPEK
            JOIN tabel_sub_aspek tsa ON tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK
            JOIN tabel_tingkat tt ON tr.ID_TINGKAT = tt.ID_TINGKAT
            JOIN tabel_posisi tp ON tr.ID_POSISI = tp.ID_POSISI
            WHERE NIM = '$nim' AND
            tr.ID_ASPEK = 5";
            $query = $this->db->query($sql);
            return $query->result();
        }
        function gettotal($nim){
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND b.STATUS = 1");
            return $query->result_array();
        }
    }
?>