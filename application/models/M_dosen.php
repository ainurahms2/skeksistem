<?php
    class M_dosen extends CI_model 
    {
        function __construct()
        {
            parent::__construct();
            $this->db = $this->load->database('default', TRUE);
            $this->db2  = $this->load->database('simpeg', TRUE);
        } 
        function getallmhs()
        {
            // $nim = $this->session->userdata('nim');
            $nip = $this->session->userdata('nip');
            $sql = "SELECT a.*,c.ASPEK,d.SUB_ASPEK, e.TINGKAT, f.POSISI, b.POIN  from input_mhs a
            JOIN tabel_rubrik b ON b.ID_RUBRIK = a.ID_RUBRIK
            JOIN tabel_aspek c ON c.ID_ASPEK = b.ID_ASPEK
            JOIN tabel_sub_aspek d ON d.ID_SUB_ASPEK = b.ID_SUB_ASPEK
            JOIN tabel_tingkat e ON e.ID_TINGKAT = b.ID_TINGKAT
            JOIN tabel_posisi f ON f.ID_POSISI = b.ID_POSISI
            WHERE NIP = '$nip' AND STATUS = 0";
            $query = $this->db->query($sql);
            return $query;
        }
        function getevalmhs()
        {
            $nip = $this->session->userdata('nip');
            $sql = "SELECT * from input_mhs
            where nip = '$nip' GROUP BY NIM";
            $query = $this->db->query($sql);
            return $query;
        }
        function getdetilnim($nim)
        {
            $sql = "SELECT b.NIM, a.*, SUM(a.POIN) AS TOTAL,c.ASPEK, b.STATUS
            FROM tabel_rubrik a
            JOIN input_mhs b ON a.ID_RUBRIK = b.ID_RUBRIK
            JOIN tabel_aspek c ON c.ID_ASPEK = a.ID_ASPEK
            WHERE b.NIM = '$nim'";
            $query = $this->db->query($sql);
            return $query->result_array();
        }
        function getaspek()
        {
            $this->db->select('*');
            $this->db->from('tabel_aspek');
            $query = $this->db->get();
            return $query->result_array();
        }
        function getsubaspek($idaspek)
        {
            $query = $this->db->query("SELECT b.* FROM aspek_subaspek a join tabel_sub_aspek b on b.ID_SUB_ASPEK = a.ID_SUB_ASPEK where a.ID_ASPEK = $idaspek");
            return $query->result();
        }
        function getaspeka($nim)
        {
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 1');
            $query = $this->db->get();
            return $query;
        }
        function getotalaspeka($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 1 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getaspekb($nim)
        {
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 2');
            $query = $this->db->get();
            return $query;
        }
        function getotalaspekb($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 2 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getaspekc($nim)
        {
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 3');
            $query = $this->db->get();
            return $query;
        }
        function getotalaspekc($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 3 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getaspekd($nim)
        {
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 4');
            $query = $this->db->get();
            return $query;
        }
        function getotalaspekd($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 4 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getaspeke($nim)
        {
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 5');
            $query = $this->db->get();
            return $query;
        }
        function getotalaspeke($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 5 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalallaspek($nim)
        {
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND b.STATUS = 1");
            return $query->result_array();
        }
        function terimaaspek($id)
        {
            $query = $this->db->query("UPDATE input_mhs SET
            STATUS = '1'
            WHERE ID_INPUT = '$id'");
        }
        function tolakaspek($id)
        {
            $query = $this->db->query("UPDATE input_mhs SET
            STATUS = '2'
            WHERE ID_INPUT = '$id'");
        }
        function updatedata($nim,$nama)
        {
            $idrubrik = $this->input->post('idrubrikhid');
            $nim = $this->input->post('nim');
            $nama = $this->input->post('namas');
            $data = array(
                'NIM' => $nim,
                'NAMA_MHS' => $nama,
                'ID_RUBRIK' => $idrubrik
            );
            $hasil = $this->db->query("UPDATE input_mhs SET NAMA_MHS = '$nama', NIM = '$nim', ID_RUBRIK  = '$idrubrik'");
        }
    }
    
?>