<?php
    class M_inputskek extends CI_model{
        function __construct()
        {
            parent::__construct();
            $this->db = $this->load->database('default', TRUE);
            $this->db2  = $this->load->database('simpeg', TRUE);
        }
        function getaspek()
        {
            $this->db->select('*');
            $this->db->from('tabel_aspek');
            $query = $this->db->get();
            return $query->result_array();
        }
        function getrubrik()
        {
            $query = $this->db->query("SELECT * FROM tabel_rubrik");
            return $query;
        }
        function getsubaspek($idaspek)
        {
            // $this->db->select('ID_SUB_ASPEK,SUB_ASPEK');
            // $this->db->from('tabel_sub_aspek');
            // $this->db->where('ID_ASPEK', $postdata['ID_ASPEK']);            
            $query = $this->db->query("SELECT b.* FROM tabel_rubrik a join tabel_sub_aspek b on b.ID_SUB_ASPEK = a.ID_SUB_ASPEK where a.ID_ASPEK = $idaspek GROUP BY b.SUB_ASPEK");
            // $query = $this->db->get();
            return $query->result();
        }
        function gettingkat($idsubaspek)
        {
            // $this->db->select('sa.id_sub_aspek, sa.id_tingkat, tg.tingkat');            
            // $this->db->from('tabel_sub_aspek');
            // $this->db->join('tabel_tingkat', 'sa.id_tingkat = tg.id_tingkat');
            // $this->db->where('sa.sub_aspek', $tingkat);            
            $query = $this->db->query("SELECT b.* FROM tabel_rubrik a join tabel_tingkat b on b.ID_TINGKAT = a.ID_TINGKAT where a.ID_SUB_ASPEK = $idsubaspek GROUP BY b.tingkat");
            // $query = $this->db->get();
            return $query->result();            
            //select sa.id_sub_aspek, sa.id_tingkat, tg.tingkat from tabel_sub_aspek sa  join tabel_tingkat tg on sa.id_tingkat = tg.id_tingkat where sa.sub_aspek = "Kegiatan Group Profesi"
        }
        function getposisi($idaspek,$idsubaspek,$idtingkat)
        {
            $query = $this->db->query("SELECT b.* FROM tabel_rubrik a join tabel_posisi b on b.ID_POSISI = a.ID_POSISI where a.id_aspek = '$idaspek' AND a.id_sub_aspek = '$idsubaspek' AND a.id_tingkat = '$idtingkat'");
            // select * from tabel_rubrik tr join tabel_posisi tp on tr.id_posisi = tp.id_posisi where tr.id_aspek = 1 AND tr.id_sub_aspek = 1 AND tr.id_tingkat = 1
            return $query->result();
        }
        function getpoin($idaspek,$idsubaspek,$idtingkat,$idposisi){
            $query = $this->db->query("SELECT POIN,ID_RUBRIK FROM tabel_rubrik where ID_ASPEK = '$idaspek' AND ID_SUB_ASPEK = '$idsubaspek' AND ID_TINGKAT = '$idtingkat' AND ID_POSISI = '$idposisi'");
            return $query->result();
        }
        function getaspeka()
        {
            $nim = $this->session->userdata('nim');
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 1');
            // SELECT * FROM input_mhs im join tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK where NIM = "H76216060" AND tr.ID_ASPEK = 1
            $query = $this->db->get();
            return $query;
        }
        function getaspekb()
        {
            $nim = $this->session->userdata('nim');
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 2');
            // SELECT * FROM input_mhs im join tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK where NIM = "H76216060" AND tr.ID_ASPEK = 1
            $query = $this->db->get();
            return $query;
        }
        function getaspekc()
        {
            $nim = $this->session->userdata('nim');
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 3');
            // SELECT * FROM input_mhs im join tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK where NIM = "H76216060" AND tr.ID_ASPEK = 1
            $query = $this->db->get();
            return $query;
        }
        function getaspekd()
        {
            $nim = $this->session->userdata('nim');
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 4');
            // SELECT * FROM input_mhs im join tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK where NIM = "H76216060" AND tr.ID_ASPEK = 1
            $query = $this->db->get();
            return $query;
        }
        function getaspeke()
        {
            $nim = $this->session->userdata('nim');
            $this->db->select('*');
            $this->db->from('input_mhs im');
            $this->db->join('tabel_rubrik tr','im.ID_RUBRIK = tr.ID_RUBRIK');
            $this->db->join('tabel_aspek ta','tr.ID_ASPEK = ta.ID_ASPEK');
            $this->db->join('tabel_sub_aspek tsa','tr.ID_SUB_ASPEK = tsa.ID_SUB_ASPEK');
            $this->db->join('tabel_tingkat tt','tr.ID_TINGKAT = tt.ID_TINGKAT');
            $this->db->join('tabel_posisi tp','tr.ID_POSISI = tp.ID_POSISI');
            $this->db->where('NIM',$nim);
            $this->db->where('tr.ID_ASPEK = 5');
            // SELECT * FROM input_mhs im join tabel_rubrik tr ON im.ID_RUBRIK = tr.ID_RUBRIK where NIM = "H76216060" AND tr.ID_ASPEK = 1
            $query = $this->db->get();
            return $query;
        }
        function simpandatamhs()
        {
            $nama = $this->session->userdata('nama');
            $nim = $this->session->userdata('nim');
            $prodi = $this->session->userdata('prodi');
            $nip = $this->session->userdata('nipdosenwali');
            //$fix=str_replace("C:/xampp/htdocs/skeksistem/uploads/","",$dir);
			$dir = $this->input->post('sertif');
            $data = array(
                'NAMA_MHS' => $nama,
                'NIM' => $nim,
                'NIP' => $nip,
                'PRODI_MHS' => $prodi,
                'ID_RUBRIK' => $this->input->post('idrubrikhid'),
                'STATUS' => 0,
                'FILE_UPLOAD' => $dir   
            );
            $hasil = $this->db->insert('input_mhs',$data);
			//print_r($this->db);
            // $query = $this->db->query("SELECT * from input_mhs where NIM = '$nim'");
        }
        
        function getotalaspeka()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 1 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalaspekb()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 2 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalaspekc()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 3 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalaspekd()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 4 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalaspeke()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a 
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND a.ID_ASPEK = 5 AND b.STATUS = 1");
            return $query->result_array();
        }
        function getotalallaspek()
        {
            $nim = $this->session->userdata('nim');
            $query = $this->db->query("SELECT SUM(a.POIN) AS Total
            FROM tabel_rubrik a
            JOIN input_mhs b
            ON a.ID_RUBRIK=b.ID_RUBRIK
            WHERE b.NIM = '$nim' AND b.STATUS = 1");
            return $query->result_array();
        }
        function deleteaspek($id)
        {
            $query = $this->db->query("DELETE FROM input_mhs WHERE ID_INPUT = '$id'");
            // return $query->result();
            // $this->db->delete('input_mhs');
            // $this->db->where('ID_INPUT', $id);
        }
    }
?>