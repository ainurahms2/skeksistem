<?php
    class M_login extends CI_model{
        function __construct()
        {
            parent::__construct();
            $this->db = $this->load->database();
            $this->db2  = $this->load->database('simpeg', TRUE);
        }
        
        function ceklogin2($username)
        {
            // $this->db2->select('nip,nama');
            // $query = $this->db2->get('tbpegawai');
            // $sql="select * from tbsimpeguser where username='$username' and password"
            $this->db2->where('username',$username);
            return $this->db2->count_all_results('tbsimpeguser');
            // return $query->result_array();
        }
		function ceklogin($username, $password)
        {
            // $this->db2->select('nip,nama');
            // $query = $this->db2->get('tbpegawai');
            // $sql="select * from tbsimpeguser where username='$username' and password"
            $this->db2->where('username',$username);
            $this->db2->where('password',$password);
            return $this->db2->count_all_results('tbsimpeguser');
            // return $query->result_array();
        }
        function getdata($username)
        {
            $this->db2->select('id_pegawai');
            $this->db2->where('username', $username);
            $query =  $this->db2->get('tbsimpeguser');
            return $query->result();
        }
        function getuser($user)
        {
            $id=$this->session->userdata('user');

            $this->db2->select('id,nip,id_tipe,nama');
            $this->db2->where('id', $user);
            $query = $this->db2->get('tbpegawai');
            return $query->result_array();   
        }
        function pengelola()
        {
            $sql = "SELECT b.id, b.kode_unit, a.nama, a.nip, b.nama_jastruk, a.id_tipe 
            FROM tbpegawai a
            JOIN m_jastruk b
            ON b.id = a.id_jastruk 
            WHERE b.id = '44'or b.id = '128' or b.id='165' or b.id='194' or b.id = '291' or b.id = '361'";
            $query = $this->db2->query($sql);
            return $query->result();
        }
        
    }
?>