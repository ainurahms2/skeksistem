-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Jan 2019 pada 13.02
-- Versi server: 10.1.19-MariaDB
-- Versi PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skeksistem`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspek_subaspek`
--

CREATE TABLE `aspek_subaspek` (
  `ID_ASPEK` int(11) NOT NULL,
  `ID_SUB_ASPEK` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aspek_subaspek`
--

INSERT INTO `aspek_subaspek` (`ID_ASPEK`, `ID_SUB_ASPEK`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(3, 13),
(4, 14),
(4, 15),
(4, 16),
(4, 17),
(4, 18),
(4, 19),
(4, 20),
(4, 21),
(4, 26),
(5, 23),
(5, 24);

-- --------------------------------------------------------

--
-- Struktur dari tabel `input_mhs`
--

CREATE TABLE `input_mhs` (
  `ID_INPUT` int(11) NOT NULL,
  `NIM` varchar(15) NOT NULL,
  `NIP` varchar(50) NOT NULL,
  `NAMA_MHS` varchar(50) NOT NULL,
  `PRODI_MHS` varchar(50) NOT NULL,
  `FILE_UPLOAD` varchar(150) DEFAULT NULL,
  `STATUS` int(11) DEFAULT NULL,
  `ID_RUBRIK` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `input_mhs`
--

INSERT INTO `input_mhs` (`ID_INPUT`, `NIM`, `NIP`, `NAMA_MHS`, `PRODI_MHS`, `FILE_UPLOAD`, `STATUS`, `ID_RUBRIK`) VALUES
(53, 'H76216060', '198808132014031001', 'MOCH AINUR RAHMAN SURYANA', 'Sistem Informasi', 'http://localhost/skeksistem/uploads/sertifH76216060/afi_mascot.jpg', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subaspek_tingkat`
--

CREATE TABLE `subaspek_tingkat` (
  `ID_SUB_ASPEK` int(11) NOT NULL,
  `ID_TINGKAT` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `subaspek_tingkat`
--

INSERT INTO `subaspek_tingkat` (`ID_SUB_ASPEK`, `ID_TINGKAT`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(4, 7),
(4, 8),
(5, 9),
(5, 10),
(5, 11),
(5, 12),
(5, 13),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(7, 53),
(7, 54),
(7, 55),
(7, 56),
(7, 57),
(7, 58),
(7, 59),
(7, 60),
(7, 61),
(7, 62),
(7, 63),
(7, 64),
(7, 65),
(7, 66),
(7, 67),
(7, 68),
(7, 69),
(7, 70),
(8, 1),
(8, 2),
(8, 4),
(8, 6),
(9, 14),
(9, 15),
(9, 16),
(9, 17),
(9, 18),
(9, 19),
(9, 20),
(9, 21),
(9, 22),
(9, 23),
(9, 24),
(9, 25),
(9, 26),
(9, 27),
(9, 28),
(9, 29),
(9, 30),
(9, 31),
(9, 32),
(9, 33),
(9, 34),
(9, 35),
(9, 36),
(9, 37),
(9, 38),
(9, 39),
(9, 71),
(10, 1),
(10, 2),
(10, 3),
(10, 4),
(11, 41),
(11, 42),
(11, 43),
(11, 44),
(11, 45),
(12, 1),
(12, 2),
(12, 3),
(12, 6),
(13, 1),
(13, 2),
(13, 3),
(13, 6),
(14, 1),
(14, 2),
(14, 3),
(14, 4),
(14, 5),
(15, 49),
(16, 1),
(16, 2),
(16, 4),
(16, 5),
(16, 6),
(17, 49),
(18, 53),
(18, 54),
(18, 55),
(18, 56),
(18, 57),
(18, 58),
(18, 59),
(18, 60),
(18, 61),
(18, 62),
(18, 63),
(18, 64),
(18, 65),
(18, 66),
(18, 67),
(18, 68),
(18, 69),
(18, 70),
(19, 53),
(19, 54),
(19, 55),
(19, 56),
(19, 57),
(19, 58),
(19, 59),
(19, 60),
(19, 61),
(19, 62),
(19, 63),
(19, 64),
(19, 65),
(19, 66),
(19, 67),
(19, 68),
(19, 69),
(19, 70),
(20, 1),
(20, 2),
(20, 3),
(20, 4),
(20, 5),
(20, 6),
(21, 1),
(21, 2),
(21, 3),
(21, 4),
(21, 5),
(21, 6),
(22, 1),
(22, 2),
(22, 3),
(22, 4),
(22, 5),
(22, 6),
(26, 49),
(23, 50),
(24, 51);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_aspek`
--

CREATE TABLE `tabel_aspek` (
  `ID_ASPEK` int(11) NOT NULL,
  `ASPEK` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_aspek`
--

INSERT INTO `tabel_aspek` (`ID_ASPEK`, `ASPEK`) VALUES
(1, 'ASPEK KEAGAMAAN DAN MORAL PANCASILA'),
(2, 'ASPEK PENALARAN DAN IDEALISME'),
(3, 'ASPEK KEPEMIMPINAN DAN LOYALITAS'),
(4, 'ASPEK PEMENUHAN BAKAT DAN MINAT'),
(5, 'ASPEK PENGABDIAN KEPADA MASYARAKAT');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_posisi`
--

CREATE TABLE `tabel_posisi` (
  `ID_POSISI` int(11) NOT NULL,
  `POSISI` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_posisi`
--

INSERT INTO `tabel_posisi` (`ID_POSISI`, `POSISI`) VALUES
(1, 'Peserta'),
(2, 'Petugas/Pembawa Acara'),
(3, 'Penceramah'),
(4, 'Konsultan'),
(5, 'Ketua'),
(6, 'Penelitian'),
(7, 'Editor'),
(8, 'Pengarang'),
(9, 'Harian/Majalah Umum'),
(10, 'Koran/Majalah Kampus Tk. Institut'),
(11, 'Koran/Majalah Kampus Tk. Fakultas'),
(12, 'Tulisan tidak dipublikasikan berupa buku'),
(13, 'Penerjemah'),
(14, 'Juara Tingkat Internasional - I'),
(15, 'Juara Tingkat Internasional - II'),
(16, 'Juara Tingkat Internasional - III'),
(17, 'Juara Tingkat Nasional - I'),
(18, 'Juara Tingkat Nasional - II'),
(19, 'Juara Tingkat Nasional - III'),
(20, 'Juara Tingkat Regional - I'),
(21, 'Juara Tingkat Regional - II'),
(22, 'Juara Tingkat Regional - III'),
(23, 'Juara Tingkat Universitas - I'),
(24, 'Juara Tingkat Universitas - II'),
(25, 'Juara Tingkat Universitas - III'),
(26, 'Juara Tingkat Fakultas - I'),
(27, 'Juara Tingkat Fakultas - II'),
(28, 'Juara Tingkat Fakultas - III'),
(29, 'Juara Tingkat Lokal - I'),
(30, 'Juara Tingkat Lokal - II'),
(31, 'Juara Tingkat Lokal - III'),
(32, 'Ketua DEMA'),
(33, 'Anggota DEMA'),
(34, 'Pengurus Harian DEMA'),
(35, 'Ketua Unit Kegiatan Mahasiswa'),
(36, 'Anggota Pengurus Unit Kegiatan Mahasiswa'),
(37, 'Ketua Senat Mahasiswa Fakultas'),
(38, 'Pengurus Harian Senat Mahasiswa Fakultas'),
(39, 'Ketua Seksi/Biro Sema Mahasiswa'),
(40, 'Anggota Pengurus Seksi/Biro Sema Eakultas'),
(41, 'Ketua Musyawarah DEMA'),
(42, 'Anggota Musyawarah DEMA'),
(43, 'Ketua Musyawarah SME, UKM, UKK, HMJ'),
(44, 'Anggota Musyawarah SMF, UKM, UKK, HMJ'),
(45, 'Ketua HMJ'),
(46, 'Pengurus Harian HMJ'),
(47, 'Anggota Pengurus HMJ'),
(48, 'Ketua Mahasiswa Angkatan Semester/HMJ'),
(49, 'Ketua Harian Angkatan Semester'),
(50, 'Ketua Pramuka'),
(51, 'Pengurus Harian Pramuka'),
(52, 'Ketua Seksi Pramuka'),
(53, 'Anggota Pengurus Pramuka'),
(54, 'Anggota Pramuka'),
(55, 'Komandan MENWA'),
(56, 'Wakil Komandan MENWA'),
(57, 'Asisten Komandan MENWA'),
(58, 'Anggota Pengurus MENWA'),
(59, 'Anggota MENWA'),
(60, 'Pengurus Harian'),
(61, 'Anggota Pengurus Lainnya'),
(62, 'Pelaksana'),
(63, 'Anggota'),
(64, 'Pelaksana Harian'),
(65, 'Inti'),
(66, 'Pengurus Lainnya'),
(72, 'Pemain Aktif Per Tahun'),
(73, 'Internasional - Ketua'),
(74, 'Internasional - Pengurus Lainnya'),
(75, 'Nasional - Ketua'),
(76, 'Nasional - Pengurus Lainnya'),
(77, 'Universitas - Ketua'),
(78, 'Universitas - Pengurus Lainnya'),
(79, 'Fakultas - Ketua'),
(80, 'Fakultas - Pengurus Lainnya'),
(81, 'Lokal - Ketua'),
(82, 'Lokal - Pengurus Lainnya'),
(83, 'Ketua Pelaksana'),
(84, 'Anggota Pelaksana'),
(85, 'Anggota Harian'),
(86, 'Internasional'),
(87, 'Nasional'),
(88, 'Regional'),
(89, 'Universitas'),
(90, 'Fakultas'),
(91, 'Lokal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_rubrik`
--

CREATE TABLE `tabel_rubrik` (
  `ID_RUBRIK` int(11) NOT NULL,
  `ID_ASPEK` int(11) DEFAULT NULL,
  `ID_SUB_ASPEK` int(11) DEFAULT NULL,
  `ID_TINGKAT` int(11) DEFAULT NULL,
  `ID_POSISI` int(11) DEFAULT NULL,
  `POIN` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_rubrik`
--

INSERT INTO `tabel_rubrik` (`ID_RUBRIK`, `ID_ASPEK`, `ID_SUB_ASPEK`, `ID_TINGKAT`, `ID_POSISI`, `POIN`) VALUES
(1, 1, 1, 1, 1, 3),
(2, 1, 1, 1, 2, 4),
(3, 1, 1, 1, 3, 6),
(4, 1, 1, 2, 1, 2),
(5, 1, 1, 2, 2, 2),
(6, 1, 1, 2, 3, 5),
(7, 1, 1, 3, 1, 1),
(8, 1, 1, 3, 2, 2),
(9, 1, 1, 3, 3, 3),
(10, 1, 1, 4, 1, 1),
(11, 1, 1, 4, 2, 2),
(12, 1, 1, 4, 3, 3),
(13, 1, 1, 5, 1, 1),
(14, 1, 1, 5, 2, 1),
(15, 1, 1, 5, 3, 2),
(16, 1, 1, 6, 1, 1),
(17, 1, 1, 6, 2, 1),
(18, 1, 1, 6, 3, 2),
(19, 1, 2, 1, 1, 7),
(20, 1, 2, 1, 2, 8),
(21, 1, 2, 1, 3, 9),
(22, 1, 2, 2, 1, 6),
(23, 1, 2, 2, 2, 7),
(24, 1, 2, 2, 3, 8),
(25, 1, 2, 3, 1, 6),
(26, 1, 2, 3, 2, 7),
(27, 1, 2, 3, 3, 8),
(28, 1, 2, 4, 1, 2),
(29, 1, 2, 4, 2, 4),
(30, 1, 2, 4, 3, 5),
(31, 1, 2, 5, 1, 2),
(32, 1, 2, 5, 2, 3),
(33, 1, 2, 5, 3, 5),
(34, 1, 2, 6, 1, 2),
(35, 1, 2, 6, 2, 3),
(36, 1, 2, 6, 3, 4),
(37, 2, 3, 1, 1, 7),
(38, 2, 3, 1, 2, 8),
(39, 2, 3, 1, 3, 9),
(40, 2, 3, 2, 1, 3),
(41, 2, 3, 2, 2, 4),
(42, 2, 3, 3, 1, 8),
(43, 2, 3, 3, 2, 7),
(44, 2, 3, 3, 3, 8),
(45, 2, 3, 4, 1, 3),
(46, 2, 3, 4, 2, 4),
(47, 2, 3, 4, 3, 5),
(48, 2, 3, 5, 1, 2),
(49, 2, 3, 5, 2, 3),
(50, 2, 3, 5, 3, 4),
(51, 2, 3, 6, 1, 2),
(52, 2, 3, 6, 2, 3),
(53, 2, 3, 6, 3, 4),
(54, 2, 4, 7, 4, 6),
(55, 2, 4, 7, 5, 5),
(56, 2, 4, 7, 6, 2),
(57, 2, 4, 7, 7, 4),
(58, 2, 4, 8, 4, 6),
(59, 2, 4, 8, 5, 3),
(60, 2, 4, 8, 6, 4),
(61, 2, 4, 8, 7, 4),
(62, 2, 5, 9, 8, 8),
(63, 2, 5, 9, 7, 4),
(64, 2, 5, 10, 9, 4),
(65, 2, 5, 10, 10, 3),
(66, 2, 5, 10, 11, 2),
(67, 2, 5, 11, 12, 2),
(68, 2, 5, 12, 13, 5),
(69, 2, 5, 12, 7, 4),
(70, 2, 5, 13, 13, 3),
(71, 2, 5, 13, 7, 2),
(72, 2, 6, 1, 1, 7),
(73, 2, 6, 1, 3, 9),
(74, 2, 6, 2, 1, 6),
(75, 2, 6, 2, 3, 8),
(76, 2, 6, 3, 1, 3),
(77, 2, 6, 3, 3, 5),
(78, 2, 6, 4, 1, 3),
(79, 2, 6, 4, 3, 5),
(80, 2, 6, 5, 1, 2),
(81, 2, 6, 5, 3, 4),
(82, 2, 7, 53, 14, 8),
(83, 2, 7, 54, 15, 7),
(84, 2, 7, 55, 16, 6),
(85, 2, 7, 56, 17, 7),
(86, 2, 7, 57, 18, 6),
(87, 2, 7, 58, 19, 5),
(88, 2, 7, 59, 20, 5),
(89, 2, 7, 60, 21, 4),
(90, 2, 7, 61, 22, 3),
(91, 2, 7, 62, 23, 5),
(92, 2, 7, 63, 24, 4),
(93, 2, 7, 64, 25, 3),
(94, 2, 7, 65, 26, 3),
(95, 2, 7, 66, 27, 2),
(96, 2, 7, 67, 28, 1),
(97, 2, 7, 68, 29, 3),
(98, 2, 7, 69, 30, 2),
(99, 2, 7, 70, 31, 1),
(100, 2, 8, 1, 1, 7),
(101, 2, 8, 1, 3, 9),
(102, 2, 8, 2, 1, 6),
(103, 2, 8, 2, 3, 8),
(104, 2, 8, 4, 1, 3),
(105, 2, 8, 4, 3, 5),
(106, 2, 8, 5, 1, 2),
(107, 2, 8, 5, 3, 4),
(108, 2, 8, 6, 1, 2),
(109, 2, 8, 6, 3, 4),
(110, 3, 9, 14, 32, 10),
(111, 3, 9, 15, 33, 8),
(112, 3, 9, 16, 34, 6),
(113, 3, 9, 17, 35, 6),
(114, 3, 9, 18, 36, 4),
(115, 3, 9, 19, 37, 8),
(116, 3, 9, 20, 38, 6),
(117, 3, 9, 21, 39, 4),
(118, 3, 9, 22, 40, 3),
(119, 3, 9, 23, 41, 3),
(120, 3, 9, 24, 42, 2),
(121, 3, 9, 25, 43, 2),
(122, 3, 9, 26, 44, 1),
(123, 3, 9, 27, 45, 4),
(124, 3, 9, 28, 46, 3),
(125, 3, 9, 29, 47, 2),
(126, 3, 9, 30, 48, 2),
(127, 3, 9, 31, 49, 0),
(128, 3, 9, 32, 50, 7),
(129, 3, 9, 33, 51, 6),
(130, 3, 9, 34, 52, 5),
(131, 3, 9, 35, 53, 4),
(132, 3, 9, 36, 54, 2),
(133, 3, 9, 37, 55, 7),
(134, 3, 9, 38, 56, 6),
(135, 3, 9, 39, 57, 5),
(136, 3, 9, 40, 58, 4),
(137, 3, 9, 71, 59, 2),
(138, 3, 10, 1, 1, 7),
(139, 3, 10, 1, 3, 9),
(140, 3, 10, 2, 1, 6),
(141, 3, 10, 2, 3, 8),
(142, 3, 10, 3, 1, 3),
(143, 3, 10, 3, 3, 5),
(144, 3, 10, 4, 1, 3),
(145, 3, 10, 4, 3, 5),
(146, 3, 10, 5, 1, 2),
(147, 3, 10, 5, 3, 4),
(148, 3, 10, 6, 1, 2),
(149, 3, 10, 6, 3, 4),
(150, 3, 11, 41, 5, 3),
(151, 3, 11, 41, 60, 2),
(152, 3, 11, 41, 61, 1),
(153, 3, 11, 42, 5, 3),
(154, 3, 11, 42, 60, 2),
(155, 3, 11, 42, 61, 1),
(156, 3, 11, 43, 5, 3),
(157, 3, 11, 43, 60, 2),
(158, 3, 11, 43, 61, 1),
(159, 3, 11, 44, 5, 3),
(160, 3, 11, 44, 60, 2),
(161, 3, 11, 44, 61, 1),
(162, 3, 11, 45, 62, 2),
(163, 3, 11, 45, 1, 1),
(164, 3, 12, 1, 5, 20),
(165, 3, 12, 1, 60, 18),
(166, 3, 12, 1, 63, 15),
(167, 3, 12, 2, 5, 15),
(168, 3, 12, 2, 60, 12),
(169, 3, 12, 2, 63, 10),
(170, 3, 12, 3, 5, 10),
(171, 3, 12, 3, 60, 7),
(172, 3, 12, 3, 63, 5),
(173, 3, 12, 6, 5, 5),
(174, 3, 12, 6, 60, 3),
(175, 3, 12, 6, 63, 2),
(176, 3, 13, 1, 5, 15),
(177, 3, 13, 1, 64, 10),
(178, 3, 13, 1, 65, 8),
(179, 3, 13, 1, 63, 6),
(180, 3, 13, 2, 5, 12),
(181, 3, 13, 2, 64, 8),
(182, 3, 13, 2, 65, 6),
(183, 3, 13, 2, 63, 4),
(184, 3, 13, 3, 5, 10),
(185, 3, 13, 3, 64, 6),
(186, 3, 13, 3, 65, 4),
(187, 3, 13, 3, 63, 2),
(188, 3, 13, 6, 5, 5),
(189, 3, 13, 6, 64, 3),
(190, 3, 13, 6, 65, 2),
(191, 3, 13, 6, 63, 1),
(192, 4, 14, 1, 5, 15),
(193, 4, 14, 1, 66, 12),
(194, 4, 14, 2, 5, 10),
(195, 4, 14, 2, 66, 8),
(196, 4, 14, 3, 5, 5),
(197, 4, 14, 3, 66, 4),
(198, 4, 14, 4, 5, 5),
(199, 4, 14, 4, 66, 4),
(200, 4, 14, 5, 5, 4),
(203, 4, 14, 5, 66, 3),
(204, 4, 15, 49, 72, 2),
(205, 4, 16, 1, 5, 15),
(206, 4, 16, 1, 66, 12),
(207, 4, 16, 2, 5, 10),
(208, 4, 16, 2, 66, 8),
(209, 4, 16, 3, 5, 8),
(210, 4, 16, 3, 66, 5),
(211, 4, 16, 4, 5, 5),
(212, 4, 16, 4, 66, 4),
(213, 4, 16, 5, 5, 4),
(214, 4, 16, 5, 66, 3),
(215, 4, 16, 6, 5, 4),
(216, 4, 16, 6, 66, 3),
(217, 4, 17, 49, 72, 2),
(218, 4, 18, 53, 14, 10),
(219, 4, 18, 54, 15, 9),
(220, 4, 18, 55, 16, 8),
(221, 4, 18, 56, 17, 8),
(222, 4, 18, 57, 18, 7),
(223, 4, 18, 58, 19, 6),
(224, 4, 18, 59, 20, 6),
(225, 4, 18, 60, 21, 5),
(226, 4, 18, 61, 22, 4),
(227, 4, 18, 62, 23, 6),
(228, 4, 18, 63, 24, 5),
(229, 4, 18, 64, 25, 4),
(230, 4, 18, 65, 26, 4),
(231, 4, 18, 66, 27, 3),
(232, 4, 18, 67, 28, 2),
(233, 4, 18, 68, 29, 4),
(234, 4, 18, 69, 30, 3),
(235, 4, 18, 70, 31, 2),
(236, 4, 19, 53, 14, 15),
(237, 4, 19, 54, 15, 14),
(238, 4, 19, 55, 16, 13),
(239, 4, 19, 56, 17, 11),
(240, 4, 19, 57, 18, 10),
(241, 4, 19, 58, 19, 9),
(242, 4, 19, 59, 20, 6),
(243, 4, 19, 60, 21, 5),
(244, 4, 19, 61, 22, 4),
(245, 4, 19, 62, 23, 6),
(246, 4, 19, 63, 24, 5),
(247, 4, 19, 64, 25, 4),
(248, 4, 19, 65, 26, 4),
(249, 4, 19, 66, 27, 3),
(250, 4, 19, 67, 28, 2),
(251, 4, 19, 68, 29, 4),
(252, 4, 19, 69, 30, 3),
(253, 4, 19, 70, 31, 2),
(254, 4, 20, 46, 86, 5),
(255, 4, 20, 46, 87, 4),
(256, 4, 20, 46, 88, 2),
(257, 4, 20, 46, 89, 2),
(258, 4, 20, 46, 90, 1),
(259, 4, 20, 46, 91, 1),
(260, 4, 21, 47, 86, 6),
(261, 4, 21, 47, 87, 5),
(262, 4, 21, 47, 88, 3),
(263, 4, 21, 47, 89, 3),
(264, 4, 21, 47, 90, 2),
(265, 4, 21, 47, 91, 2),
(266, 4, 22, 1, 5, 15),
(267, 4, 22, 1, 66, 12),
(268, 4, 22, 2, 5, 10),
(269, 4, 22, 2, 66, 8),
(270, 4, 22, 3, 5, 10),
(271, 4, 22, 3, 66, 4),
(272, 4, 22, 4, 5, 5),
(273, 4, 22, 4, 66, 4),
(274, 4, 22, 5, 5, 4),
(275, 4, 22, 5, 66, 3),
(276, 4, 22, 6, 5, 4),
(277, 4, 22, 6, 66, 3),
(278, 4, 22, 49, 72, 2),
(279, 5, 23, 50, 83, 3),
(280, 5, 23, 50, 84, 2),
(281, 5, 24, 51, 83, 3),
(282, 5, 24, 51, 84, 2),
(283, 5, 25, 52, 5, 4),
(284, 5, 25, 52, 60, 3),
(285, 5, 25, 52, 85, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_sub_aspek`
--

CREATE TABLE `tabel_sub_aspek` (
  `ID_SUB_ASPEK` int(11) NOT NULL,
  `SUB_ASPEK` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_sub_aspek`
--

INSERT INTO `tabel_sub_aspek` (`ID_SUB_ASPEK`, `SUB_ASPEK`) VALUES
(1, 'Peringatan Hari Besar Islam, Kegiatan Keagaman, Kegiatan Moral Pancasila yang bersifat ceramah/upacara.'),
(2, 'Peringatan Hari Besar Islam, Kegiatan, Moral Pancasila yang bersifat'),
(3, 'Kegiatan diskusi/simposium/lokakarya dan semacamnya'),
(4, 'Kegiatan Penelitian '),
(5, 'Penulisan Ilmiah'),
(6, 'Latihan Karya Tulis Ilmiah'),
(7, 'Prestasi Karya Tulis Ilmiah'),
(8, 'Latihan/Penataran Penelitian'),
(9, 'Kepemimpinan Mahasiswa per periode'),
(10, 'Latihan Kepemimpinan Mahasiswa'),
(11, 'Usaha-Usaha Kesejahteraan'),
(12, 'Kepemimpinan Dalam Masyarakat/Agama/Bangsa/Negara'),
(13, 'Kepanitiaan'),
(14, 'Olah Raga-Pengurus Tim Aktif Per Tahun '),
(15, 'Olah Raga-Pemain Aktif Per Tahun'),
(16, 'Kesenian-Pengurus Aktif Per Tahun'),
(17, 'Kesenian-Pemain Aktif Per Tahun'),
(18, 'Pencapaian Prestasi-Juara Beregu'),
(19, 'Pencapaian Prestasi-Juara Perorangan'),
(20, 'Pementasan/Invitasi Biaya Tanpa Kejuaraan-Beregu'),
(21, 'Pementasan/Invitasi Biaya Tanpa Kejuaraan-Perorangan'),
(22, 'Pementasan/Invitasi Biaya Tanpa Kejuaraan-Group Profesi-Tim Aktif Per Tahun'),
(23, 'Usaha Bantuan terhadap bencana alam / musibah'),
(24, 'Usaha Pembinaan terhadap Masyarakat'),
(25, 'Usaha bantuan konsultasi keagamaan lainnya'),
(26, 'Pementasan/Invitasi Biaya Tanpa Kejuaraan-Group Profesi-Pemain Aktif Per Tahun');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_tingkat`
--

CREATE TABLE `tabel_tingkat` (
  `ID_TINGKAT` int(11) NOT NULL,
  `TINGKAT` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_tingkat`
--

INSERT INTO `tabel_tingkat` (`ID_TINGKAT`, `TINGKAT`) VALUES
(1, 'Tingkat Internasional'),
(2, 'Tingkat Nasional'),
(3, 'Tingkat Regional'),
(4, 'Tingkat Universitas'),
(5, 'Tingkat Fakultas'),
(6, 'Tingkat Lokal'),
(7, 'Penelitian Kelompok'),
(8, 'Penelitian Individual'),
(9, 'Berupa Buku'),
(10, 'Tulisan di mass media'),
(11, 'Tulisan tidak dipublikasikan berupa buku'),
(12, 'Penerjemah yang dipublikasikan berupa buku'),
(13, 'Penerjemah tidak  dipublikasikan berupa buku'),
(14, 'Ketua DEMA'),
(15, 'Anggota DEMA'),
(16, 'Pengurus Harian DEMA'),
(17, 'Ketua Unit Kegiatan Mahasiswa'),
(18, 'Anggota Pengurus Unit Kegiatan Mahasiswa'),
(19, 'Ketua Senat Mahasiswa Fakultas'),
(20, 'Pengurus Harian Senat Mahasiswa Fakultas'),
(21, 'Ketua Seksi/Biro Sema Mahasiswa'),
(22, 'Anggota Pengurus Seksi/Biro Sema Eakultas'),
(23, 'Ketua Musyawarah DEMA'),
(24, 'Anggota Musyawarah DEMA'),
(25, 'Ketua Musyawarah SME, UKM, UKK, HMJ'),
(26, 'Anggota Musyawarah SMF, UKM, UKK, HMJ'),
(27, 'Ketua HMJ'),
(28, 'Pengurus Harian HMJ'),
(29, 'Anggota Pengurus HMJ'),
(30, 'Ketua Mahasiswa Angkatan Semester/HMJ'),
(31, 'Ketua Harian Angkatan Semester'),
(32, 'Ketua Pramuka'),
(33, 'Pengurus Harian Pramuka'),
(34, 'Ketua Seksi Pramuka'),
(35, 'Anggota Pengurus Pramuka'),
(36, 'Anggota Pramuka'),
(37, 'Komandan MENWA'),
(38, 'Wakil Komandan MENWA'),
(39, 'Asisten Komandan MENWA'),
(40, 'Anggota Pengurus MENWA'),
(41, 'Usaha Asrama Mahasiswa'),
(42, 'Koperasi Mahasiswa'),
(43, 'Usaha Bimbingan dan Penyuluhan Mahasiswa'),
(44, 'Usaha Poliklinik Mahasiswa'),
(45, 'Usaha Rekreasi Mahasiswa'),
(46, 'Juara Beregu'),
(47, 'Juara Perorangan'),
(48, 'Pengurus Tim Aktif Per Tahun'),
(49, 'Pemain Aktif Per Tahun'),
(50, 'Usaha Bantuan terhadap bencana alam / musibah '),
(51, 'Usaha Pembinaan terhadap Masyarakat'),
(52, 'Usaha bantuan konsultasi keagamaan lainnya'),
(53, 'Juara Tingkat Internasional - I'),
(54, 'Juara Tingkat Internasional - II'),
(55, 'Juara Tingkat Internasional - III'),
(56, 'Juara Tingkat Nasional - I'),
(57, 'Juara Tingkat Nasional - II'),
(58, 'Juara Tingkat Nasional - III'),
(59, 'Juara Tingkat Regional - I'),
(60, 'Juara Tingkat Regional - II'),
(61, 'Juara Tingkat Regional - III'),
(62, 'Juara Tingkat Universitas - I'),
(63, 'Juara Tingkat Universitas - II'),
(64, 'Juara Tingkat Universitas - III'),
(65, 'Juara Tingkat Fakultas - I'),
(66, 'Juara Tingkat Fakultas - II'),
(67, 'Juara Tingkat Fakultas - III'),
(68, 'Juara Tingkat Lokal - I'),
(69, 'Juara Tingkat Lokal - II'),
(70, 'Juara Tingkat Lokal - III'),
(71, 'Anggota Menwa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tingkat_posisi`
--

CREATE TABLE `tingkat_posisi` (
  `ID_TINGKAT` int(11) NOT NULL,
  `ID_POSISI` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tingkat_posisi`
--

INSERT INTO `tingkat_posisi` (`ID_TINGKAT`, `ID_POSISI`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 2),
(4, 3),
(5, 1),
(5, 2),
(5, 3),
(6, 1),
(6, 2),
(6, 3),
(7, 4),
(7, 5),
(7, 6),
(7, 7),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(9, 8),
(9, 7),
(10, 9),
(10, 10),
(10, 11),
(11, 12),
(12, 13),
(12, 7),
(13, 13),
(13, 7),
(14, 32),
(15, 33),
(16, 34),
(17, 35),
(18, 36),
(19, 37),
(20, 38),
(21, 39),
(22, 40),
(23, 41),
(24, 42),
(25, 43),
(26, 44),
(27, 45),
(28, 46),
(29, 47),
(30, 48),
(31, 49),
(32, 50),
(33, 51),
(34, 52),
(35, 53),
(36, 54),
(37, 55),
(38, 56),
(39, 57),
(40, 58),
(41, 5),
(41, 60),
(41, 61),
(42, 5),
(42, 60),
(42, 61),
(43, 5),
(43, 60),
(43, 61),
(44, 5),
(44, 60),
(44, 61),
(45, 62),
(45, 1),
(1, 5),
(1, 60),
(1, 63),
(2, 5),
(2, 60),
(2, 63),
(3, 5),
(3, 60),
(3, 63),
(6, 5),
(6, 60),
(6, 63),
(1, 64),
(1, 65),
(2, 64),
(2, 65),
(3, 64),
(3, 65),
(6, 64),
(6, 65),
(48, 73),
(48, 74),
(48, 75),
(48, 76),
(48, 77),
(48, 78),
(49, 72),
(48, 81),
(48, 82),
(50, 83),
(50, 84),
(51, 83),
(51, 84),
(52, 5),
(52, 60),
(52, 85),
(53, 14),
(54, 15),
(55, 16),
(56, 17),
(57, 18),
(58, 19),
(59, 20),
(60, 21),
(61, 22),
(62, 23),
(63, 24),
(64, 25),
(65, 26),
(66, 27),
(67, 28),
(68, 29),
(69, 30),
(70, 31),
(71, 59),
(46, 14),
(46, 15),
(46, 16),
(46, 17),
(46, 18),
(46, 19),
(46, 20),
(46, 21),
(46, 22),
(46, 23),
(46, 24),
(46, 25),
(46, 26),
(46, 27),
(46, 28),
(46, 29),
(46, 30),
(46, 31),
(47, 14),
(47, 15),
(47, 16),
(47, 17),
(47, 18),
(47, 19),
(47, 20),
(47, 21),
(47, 22),
(47, 23),
(47, 24),
(47, 25),
(47, 26),
(47, 27),
(47, 28),
(47, 29),
(47, 30),
(46, 86),
(46, 87),
(46, 88),
(46, 89),
(46, 90),
(46, 91),
(47, 86),
(47, 87),
(47, 88),
(47, 89),
(47, 90);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aspek_subaspek`
--
ALTER TABLE `aspek_subaspek`
  ADD KEY `ID_SUB_ASPEK` (`ID_SUB_ASPEK`),
  ADD KEY `ID_ASPEK` (`ID_ASPEK`);

--
-- Indeks untuk tabel `input_mhs`
--
ALTER TABLE `input_mhs`
  ADD PRIMARY KEY (`ID_INPUT`),
  ADD UNIQUE KEY `ID_INPUT` (`ID_INPUT`),
  ADD KEY `ID_RUBRIK` (`ID_RUBRIK`);

--
-- Indeks untuk tabel `subaspek_tingkat`
--
ALTER TABLE `subaspek_tingkat`
  ADD KEY `ID_SUB_ASPEK` (`ID_SUB_ASPEK`),
  ADD KEY `ID_TINGKAT` (`ID_TINGKAT`);

--
-- Indeks untuk tabel `tabel_aspek`
--
ALTER TABLE `tabel_aspek`
  ADD PRIMARY KEY (`ID_ASPEK`),
  ADD UNIQUE KEY `TABEL_ASPEK_PK` (`ID_ASPEK`);

--
-- Indeks untuk tabel `tabel_posisi`
--
ALTER TABLE `tabel_posisi`
  ADD PRIMARY KEY (`ID_POSISI`),
  ADD UNIQUE KEY `TABEL_POSISI_PK` (`ID_POSISI`);

--
-- Indeks untuk tabel `tabel_rubrik`
--
ALTER TABLE `tabel_rubrik`
  ADD PRIMARY KEY (`ID_RUBRIK`),
  ADD UNIQUE KEY `TABEL_RUBRIK_PK` (`ID_RUBRIK`),
  ADD KEY `RELATIONSHIP_6_FK` (`ID_SUB_ASPEK`),
  ADD KEY `RELATIONSHIP_7_FK` (`ID_POSISI`),
  ADD KEY `ID_ASPEK` (`ID_ASPEK`),
  ADD KEY `ID_TINGKAT` (`ID_TINGKAT`);

--
-- Indeks untuk tabel `tabel_sub_aspek`
--
ALTER TABLE `tabel_sub_aspek`
  ADD PRIMARY KEY (`ID_SUB_ASPEK`);

--
-- Indeks untuk tabel `tabel_tingkat`
--
ALTER TABLE `tabel_tingkat`
  ADD PRIMARY KEY (`ID_TINGKAT`),
  ADD UNIQUE KEY `TABEL_TINGKAT_PK` (`ID_TINGKAT`);

--
-- Indeks untuk tabel `tingkat_posisi`
--
ALTER TABLE `tingkat_posisi`
  ADD KEY `ID_TINGKAT` (`ID_TINGKAT`),
  ADD KEY `ID_POSISI` (`ID_POSISI`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `input_mhs`
--
ALTER TABLE `input_mhs`
  MODIFY `ID_INPUT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT untuk tabel `tabel_posisi`
--
ALTER TABLE `tabel_posisi`
  MODIFY `ID_POSISI` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT untuk tabel `tabel_rubrik`
--
ALTER TABLE `tabel_rubrik`
  MODIFY `ID_RUBRIK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=286;

--
-- AUTO_INCREMENT untuk tabel `tabel_sub_aspek`
--
ALTER TABLE `tabel_sub_aspek`
  MODIFY `ID_SUB_ASPEK` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `tabel_tingkat`
--
ALTER TABLE `tabel_tingkat`
  MODIFY `ID_TINGKAT` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `aspek_subaspek`
--
ALTER TABLE `aspek_subaspek`
  ADD CONSTRAINT `aspek_subaspek_ibfk_2` FOREIGN KEY (`ID_SUB_ASPEK`) REFERENCES `tabel_sub_aspek` (`ID_SUB_ASPEK`),
  ADD CONSTRAINT `aspek_subaspek_ibfk_3` FOREIGN KEY (`ID_ASPEK`) REFERENCES `tabel_aspek` (`ID_ASPEK`);

--
-- Ketidakleluasaan untuk tabel `input_mhs`
--
ALTER TABLE `input_mhs`
  ADD CONSTRAINT `input_mhs_ibfk_1` FOREIGN KEY (`ID_RUBRIK`) REFERENCES `tabel_rubrik` (`ID_RUBRIK`);

--
-- Ketidakleluasaan untuk tabel `subaspek_tingkat`
--
ALTER TABLE `subaspek_tingkat`
  ADD CONSTRAINT `subaspek_tingkat_ibfk_1` FOREIGN KEY (`ID_SUB_ASPEK`) REFERENCES `tabel_sub_aspek` (`ID_SUB_ASPEK`),
  ADD CONSTRAINT `subaspek_tingkat_ibfk_2` FOREIGN KEY (`ID_TINGKAT`) REFERENCES `tabel_tingkat` (`ID_TINGKAT`);

--
-- Ketidakleluasaan untuk tabel `tabel_rubrik`
--
ALTER TABLE `tabel_rubrik`
  ADD CONSTRAINT `tabel_rubrik_ibfk_5` FOREIGN KEY (`ID_ASPEK`) REFERENCES `tabel_aspek` (`ID_ASPEK`),
  ADD CONSTRAINT `tabel_rubrik_ibfk_6` FOREIGN KEY (`ID_SUB_ASPEK`) REFERENCES `tabel_sub_aspek` (`ID_SUB_ASPEK`),
  ADD CONSTRAINT `tabel_rubrik_ibfk_7` FOREIGN KEY (`ID_TINGKAT`) REFERENCES `tabel_tingkat` (`ID_TINGKAT`),
  ADD CONSTRAINT `tabel_rubrik_ibfk_8` FOREIGN KEY (`ID_POSISI`) REFERENCES `tabel_posisi` (`ID_POSISI`);

--
-- Ketidakleluasaan untuk tabel `tingkat_posisi`
--
ALTER TABLE `tingkat_posisi`
  ADD CONSTRAINT `tingkat_posisi_ibfk_1` FOREIGN KEY (`ID_TINGKAT`) REFERENCES `tabel_tingkat` (`ID_TINGKAT`),
  ADD CONSTRAINT `tingkat_posisi_ibfk_2` FOREIGN KEY (`ID_POSISI`) REFERENCES `tabel_posisi` (`ID_POSISI`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
