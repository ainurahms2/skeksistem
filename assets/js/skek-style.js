$(document).ready(function () {
    $('#tabelmhs').DataTable({
        "bFilter": true,
        "bPaginate": true,
        "bSort" : true,
        "bInfo": true
    });
    $('#tabelaspekmhs').DataTable({
        "bFilter": false,
        "bPaginate": false,
        "bSort" : false,
        "bInfo": false
    });
    $('#tabeldsn').DataTable({
        "bFilter": true,
        "bPaginate": false,
        "bSort" : true,
        "bInfo": false
    });
    $('#tabelvalid').DataTable({
        "bFilter": true,
        "bPaginate": true,
        "bSort" : true,
        "bInfo": true
    });
    $('#tabeleval').DataTable({
        "bFilter": true,
        "bPaginate": true,
        "bSort" : true,
        "bInfo": true
    });
    $('#tabelpgw').DataTable({
        "bFilter": true,
        "bPaginate": true,
        "bSort" : true,
        "bInfo": true
    });
    $('#tabelperskek').DataTable({
        "bFilter": true,
        "bPaginate": true,
        "bSort" : true,
        "bInfo": true
    });
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});